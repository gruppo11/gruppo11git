package testing;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.Test;

import classes.AdminInterface;
import classes.CostInterface;
import classes.Customer;
import classes.CustomerInterface;
import classes.LoginInterface;
import classes.PhaseInterface;
import classes.Project;
import classes.ProjectInterface;
import classes.ProposteMaterialiInterface;
import classes.StandProject;
import client.FileManagerClient;
import client.MainWindow;
import server.LoginRmi;



public class LoginTest {
	
	
	
	private String _address="rmi://localhost:1099";
	private String _dataUser="root";
	private String _dataPwd="root";
	private String _dataHost="localhost";
	private String _dataName="mydb9";
	private String _connectionString ="jdbc:mysql://localhost/mydb9";

	@SuppressWarnings("deprecation")
	@Test
	public void test() {
		
//*************************
		
//------GENERO CONNESSIONE AL DATABASE --------		
		
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(_connectionString,_dataUser,_dataPwd);
		} catch (SQLException e2) {
		
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
		
			e1.printStackTrace();
		}

    	AdminInterface adminInt=null;
		try {
			adminInt = (AdminInterface)Naming.lookup(_address+"/AdminInt");
		} catch (MalformedURLException e3) {
		
			e3.printStackTrace();
		} catch (RemoteException e3) {
		
			e3.printStackTrace();
		} catch (NotBoundException e3) {
		
			e3.printStackTrace();
		}
		
		//**********************************************************
		
		
		try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			/*
			command.executeUpdate("DELETE FROM phases ");
			command.executeUpdate("DELETE FROM projects ");
			command.executeUpdate("DELETE FROM users ");
	*/
			command.executeUpdate("DELETE FROM programma ");
			command.executeUpdate("DELETE FROM materialiProgetto ");
			command.executeUpdate("DELETE FROM materiali ");
			command.executeUpdate("DELETE FROM personale ");
			command.executeUpdate("DELETE FROM servizi ");
			command.executeUpdate("DELETE FROM voiceOfCost ");
			
			command.executeUpdate("DELETE FROM proposta ");
			command.executeUpdate("DELETE FROM propostaStand ");
			
			command.executeUpdate("DELETE FROM phases ");
			command.executeUpdate("DELETE FROM standProject ");
			command.executeUpdate("DELETE FROM projects ");
			
			command.executeUpdate("DELETE FROM users ");
			command.executeUpdate("DELETE FROM extuser ");
			command.executeUpdate("DELETE FROM customers ");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		//***********************************************************
		
		
		
		Boolean b = false;
		try {
			b=adminInt.writeOnUsersSql(_connectionString, _dataUser, _dataPwd, "admin", "admin", "a");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("aggiunto admin boolean= "+b);
		
		//****************************************************************************
		String pwdOut ="";
	  	LoginInterface LoginInt=null;
			try {
				LoginInt = (LoginInterface)Naming.lookup(_address+"/LoginInt");
			} catch (MalformedURLException e3) {
			
				e3.printStackTrace();
			} catch (RemoteException e3) {
			
				e3.printStackTrace();
			} catch (NotBoundException e3) {
			
				e3.printStackTrace();
			}
			
			char[] pwdchar={'a','d','m','i','n'};
			
			try {
				pwdOut= LoginInt.setLoginFromData(_connectionString, _dataUser, _dataPwd, "admin", pwdchar, 0);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String pwdTemp = null;
			pwdTemp =pwdTemp.valueOf(pwdchar);
			System.out.println("pwdOut= "+pwdOut+" pwdTemp= "+pwdTemp);
			
			assertEquals(pwdOut, pwdTemp);
			
	}
	}
		