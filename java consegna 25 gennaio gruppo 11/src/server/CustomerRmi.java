package server;
import java.sql.*;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import classes.Customer;
import classes.CustomerInterface;

public class CustomerRmi extends UnicastRemoteObject implements CustomerInterface, Serializable{
    public CustomerRmi() throws RemoteException 
    {
        
    }

	public boolean addCustomer(String connectionString, String dataUser,
			String dataPwd, Customer customer) throws RemoteException {
		// TODO Auto-generated method stub
		
		System.out.println("entrato in addcustomer");
//------GENERO CONNESSIONE AL DATABASE --------		
		
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
   /* 	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    */	
    
    	String values= ("('"+customer.get_idCustomer()+"','"+customer.get_mail()+"','"+ customer.get_cell() + "','"+ customer.get_contact()+"')");
    	try {
    		System.out.println("sto per scrivere customer in database");
    		command.execute("INSERT INTO customers (idcustomer,mail,cell,contact) VALUES"+values);
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean deleteCustomer(String connectionString, String dataUser,
			String dataPwd, String idcustomer) throws RemoteException {
		// TODO Auto-generated method stub
		
//------GENERO CONNESSIONE AL DATABASE --------		
		
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
		
		Statement command = null;
		
		
		try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			command.executeQuery("DELETE FROM customers WHERE idcustomer = "+idcustomer);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;
	}

	public String getCustomers(String connectionString, String dataUser,
			String dataPwd) {

		
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	String[] out=null;
    	String stringaout=null;
    	
    	int i=0;
    		try {
    			
	 		data=command.executeQuery("SELECT idcustomer  from customers  "); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			
	 			out[i]=data.getString("idcustomer");
	 			i=i+1;
	 			}
	 		
	 		while (data.next()){
	 			out[i]=data.getString("idcustomer");
	 			i=i+1;
 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    		stringaout=FileManager.fromStringArrayToString(out);
    	
    
			return stringaout;	
		
		
	}

	public ArrayList<String> getCustomersStringList(String connectionString,
			String dataUser, String dataPwd) throws RemoteException {
		
		System.out.println("entro in getcustomerlist"	);
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPwd);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	ArrayList<String> out= new ArrayList<String>();
    	String prov="";
    	
    	int i=0;
    		try {
    		System.out.println("sto per interrogare database per customer list"	);
	 		data=command.executeQuery("SELECT  idcustomer  FROM customers  "); 
	 		System.out.println("interrogazione svolta (sono in interfaccia) ");
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			System.out.println("sono in data.first() interafccia ");
	 			System.out.println("data.getString(idcustomer) "+data.getString("idcustomer"));
	 			prov =prov+data.getString("idcustomer");
	 			out.add(prov);
	 			prov="";
	 			System.out.println("fatto data.first()get string interafccia ");
	 			i=i+1;
	 			}
	 		System.out.println("out[0] interfaccia ");
	 		while (data.next()){
	 			System.out.println("sono in data.next() interafccia ");
	 			prov =prov+data.getString("idcustomer");
	 			out.add(prov);
	 			prov="";
	 			System.out.println("fatto data.next()get string interafccia ");
	 			i=i+1;
 			}
	 			
	 		System.out.println("vettore out creato interafccia ");
	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    		
    	
    
			return out;	
	}

	public String prova(String connectionString, String dataUser, String dataPwd)
			throws RemoteException {
		System.out.println("sono in prova");
		
		// TODO Auto-generated method stub
		return "dieci";
	}
}