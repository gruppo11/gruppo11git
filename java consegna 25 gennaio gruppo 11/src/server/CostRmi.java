package server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import classes.CostInterface;
import classes.VoiceOfcost;


public class CostRmi extends UnicastRemoteObject implements CostInterface, Serializable{
    public CostRmi() throws RemoteException 
    {
        
    }

	public boolean addVoiceOfCost(String connectionString, String dataUser,
			String dataPassword, int idProject, int idPhase, int idVoiceOfCost,
			double amount, String type, String description)
			throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
   // int val=1;
    	String values= ("("+Integer.toString(idProject)+","+Integer.toString(idPhase)+","+Integer.toString(idVoiceOfCost)+","+ String.valueOf(amount) + ",'"+ type+ "','"+ description+"')");
    	try {
    		
    		command.execute("INSERT INTO voiceOfCost (idproject, idphase,idvoiceOfCost,amount,type,description) VALUES"+values);
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
		
		
		
		// TODO Auto-generated method stub
		
	}

	public boolean deleteVoiceOfCost(String connectionString, String dataUser,
			String dataPassword, int idProject, int idPhase, int idVoiceOfCost)
			throws RemoteException {
		
//------GENERO CONNESSIONE AL DATABASE --------		
		
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
		
		Statement command = null;
		
		
		try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			command.executeUpdate("DELETE FROM voiceOfCost WHERE idvoiceOfCost = "+Integer.toString(idVoiceOfCost)+" and idphase= "+ Integer.toString(idPhase)+" and idproject = "+ Integer.toString(idProject));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;
	}

	public double getTotalCost(String connectionString, String dataUser,
			String dataPassword, int idProject) throws RemoteException {

    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	ResultSet data1 = null;
    	double out=0;
    		try {
    			
	 		data=command.executeQuery("SELECT sum(amount) as total from phases JOIN voiceOfCost  on phases.idphase = voiceOfCost.idphase WHERE voiceOfCost.idproject=" +Integer.toString(idProject)); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out=out+data.getDouble("total");
	 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
    
    		try {
				data1=command.executeQuery("SELECT sum(quantita*costo)as total from materiali JOIN materialiProgetto  on materialiProgetto.idmateriale = materiali.idmateriale WHERE idproject="+Integer.toString(idProject));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		
    		try {
				if (data1.first()){  //controllo che user non esisti di gia
					out=out+data1.getDouble("total");
					}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
				
		return out;
	}

	public double getMaterialCost(String connectionString, String dataUser,
			String dataPassword, int idProject) throws RemoteException {

    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	ResultSet data1 = null;
    	double out=0;
    		
    			
		
    
    		try {
				data1=command.executeQuery("SELECT sum(quantita*costo)as total from materiali JOIN materialiProgetto  on materialiProgetto.idmateriale = materiali.idmateriale WHERE idproject="+Integer.toString(idProject));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		
    		try {
				if (data1.first()){  
					out=out+data1.getDouble("total");
					}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return out;

		
	}

	public double getEmployeeCostOfPhase(String connectionString, String dataUser,
			String dataPassword, int idProject, int idPhase)
			throws RemoteException {
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	
    	double out=0;
    		try {
    			
	 		data=command.executeQuery("SELECT sum(amount) as total from phases JOIN voiceOfCost  on phases.idphase = voiceOfCost.idphase WHERE voiceOfCost.idproject = " +Integer.toString(idProject)+ " and voiceOfCost.idphase = "+Integer.toString(idPhase)+" and type = 'employee'"); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out=out+data.getDouble("total");
	 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return out;	
	}

	public double getOtherCostOfPhase(String connectionString, String dataUser,
			String dataPassword, int idProject, int idPhase)
			throws RemoteException {
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	
    	double out=0;
    		try {
    			
	 		data=command.executeQuery("SELECT sum(amount) as total from phases JOIN voiceOfCost  on phases.idphase = voiceOfCost.idphase WHERE voiceOfCost.idproject =" +Integer.toString(idProject)+ " and voiceOfCost.idphase = "+Integer.toString(idPhase)+" and type = 'other'"); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out=out+data.getDouble("total");
	 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return out;	
	}

	public ArrayList<Integer> getIdCost(String connectionString, String dataUser,
			String dataPassword, int idProject, int idPhase)
			throws RemoteException {
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	
    	ArrayList<Integer> out=new ArrayList<Integer>();
    	
    		try {
    			
	 		data=command.executeQuery("SELECT idvoiceOfCost from voiceOfCost   WHERE  idphase = "+Integer.toString(idPhase)+ " and idproject = "+ idProject); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out.add(data.getInt("idvoiceOfCost")) ;
	 			
	 			}
	 		while (data.next()){
	 			out.add(data.getInt("idvoiceOfCost")) ;
	 		
	 			
	 		}

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		
		
		
		// TODO Auto-generated method stub
		return out;
	}

	public VoiceOfcost getCostById(String connectionString, String dataUser,
			String dataPassword, int idVoiceOfCost, int idPhase, int idProject)
			throws RemoteException {
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	VoiceOfcost out=null;
    	
    		try {
    			
	 		data=command.executeQuery("SELECT * from voiceOfCost  WHERE  idphase = "+Integer.toString(idPhase)+" and idvoiceOfCost = "+Integer.toString(idVoiceOfCost)+" and idproject = "+Integer.toString(idProject)); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			
	 			out=new VoiceOfcost(data.getInt("idvoiceOfCost"),data.getInt("idphase"),data.getDouble("amount"), data.getString("type"), data.getString("description"));
	 			}
	 	

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		
		
		
		// TODO Auto-generated method stub
		return out;
	}
}
