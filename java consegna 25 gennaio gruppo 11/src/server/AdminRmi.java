package server;



import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;

import classes.AdminInterface;

public class AdminRmi extends UnicastRemoteObject implements AdminInterface, Serializable{
    public AdminRmi() throws RemoteException 
    {
        
    }
    
	public boolean deleteOnUserSql(String connectionString, String dataUser, String dataPassword, String user) throws RemoteException {
		// TODO Auto-generated method stub
		Connection connection = null;
		Statement command = null;
	
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		
		try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			command.executeQuery("DELETE FROM users WHERE idusers = "+user);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;
	}
    
    public boolean writeOnUsersSql (String connectionString, String dataUser, String dataPassword, String user, String pwd, String access)throws RemoteException {
    	System.out.println("sono in writeonusersql");
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    		try {
    			
	 		data=command.executeQuery("SELECT idusers FROM users"); 
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			System.out.println("entro in controllo esistenza1");
	 			if (user.contentEquals(data.getString("idusers"))){
	 				return false;
	 			}
	 			
	 			while (data.next()){
	 				System.out.println("entro in controllo esistenza2");
	 				if (user.contentEquals(data.getString("idusers"))){
		 				return false;
		 			}
		 			
	 			}
	 		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
       	// eseguire qua la conversione in sha256
    	String pwdTemp="";
    	/*
    	pwdTemp=pwdTemp.valueOf(pwd);
    	String pwdsalata = pwdTemp+"xj34876hs";
    	MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	byte[] hash = digest.digest(pwdsalata.getBytes(StandardCharsets.UTF_8));
		pwdTemp=hash.toString();*/
		// fine conversione in sha256
		
	pwdTemp=pwdTemp.valueOf(pwd);
    	
    	
    	
    	String values= ("('"+user+"','"+pwdTemp+"','"+access+"')");
    	try {
    		System.out.println("scrivo su db");
    		command.execute("INSERT INTO users (idusers,pwd,accessType) VALUES"+values);
    		System.out.println("ho scritto");
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
    	
    }
    	


	public boolean addUser(String user, String pwd, String access)
			throws RemoteException {
		if (user.contentEquals("")||pwd.contentEquals("")){
			return false;
		}
		
		ArrayList<ArrayList<String>> lista = new ArrayList<ArrayList<String>>();
		try {
			System.out.println("leggo da file csv... ");
			lista= FileManager.readFile("users.csv");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("lista... "+lista);
		
		for (int i=0; i<lista.size(); i=i+1){
			if (user.contentEquals(lista.get(i).get(0))){
				System.out.println("nome gia presente... ");
				return false; //esiste gia un user
			}
		}
		System.out.println("creao lista da inserire in lista... ");
		ArrayList<String> userPwdAcc=new ArrayList<String>();
		userPwdAcc.add(user);
		userPwdAcc.add(pwd);
		userPwdAcc.add(access);
		
		System.out.println("userPwdAcc... " + userPwdAcc);
		
		lista.add(userPwdAcc);//aggiunto
		
		FileManager.writeFileFromArrayList("users", "csv", lista);
		
		return true;
		
	}

	@SuppressWarnings("null")
	public boolean deleteUser(String user) throws RemoteException {
		// TODO Auto-generated method stub
		if (user.contentEquals("")){
			return false;
		}
		ArrayList<ArrayList<String>> lista = new ArrayList<ArrayList<String>>();
		try {
			lista= FileManager.readFile("users.csv");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean esistentUser=false;
		int indiceUser=-1;
		for (int i=0; i<lista.size(); i=i+1){
			if (user.contentEquals(lista.get(i).get(0))){
				esistentUser=true;
				indiceUser=i;
				i=lista.size();
			}
		}
		if (!esistentUser){
			return false; //user non trovato
		}
		
		else { //elimino user
			ArrayList<ArrayList<String>> lista_copy = new ArrayList<ArrayList<String>>(); //copio fino a indice-1
			for (int i=0; i<indiceUser; i=i+1){
				lista_copy.add(lista.get(i));
			}
			if (indiceUser!=lista.size()-1){ //copio da indice+1 fino a fine
				for (int i=indiceUser+1; i<lista.size(); i=i+1){
					lista_copy.add(lista.get(i));
				}
			}
			
			FileManager.writeFileFromArrayList("users", "csv", lista_copy);
			
		}
		
		
		return true;
	}

	public ArrayList<String> getDipendentiManager(String connectionString,
			String dataUser, String dataPassword, char dipMan)
			throws RemoteException {
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	ArrayList<String> out=new ArrayList<String>();
    		try {
    			
	 		data=command.executeQuery("SELECT idusers FROM users WHERE accessType ='"+dipMan+"'"); 
	 		int i=0;
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out.add(data.getString("idusers"));
	 			i=i+1;
	 			}
	 			
	 			while (data.next()){
	 				out.add(data.getString("idusers"));
		 			i=i+1;
	 			}
	 			
 			
 		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
			return out;
	}

	public boolean setExtPrivileges(String connectionString, String dataUser,
			String dataPassword,String idUser, int maxAcc) throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
    
    	String values= ("("+Integer.toString(0)+","+Integer.toString(maxAcc)+",'"+ idUser + "')");
    	try {
    		
    		command.execute("INSERT INTO extuser (numberOfAccess,macAccess,idusers) VALUES"+values+"where idusers='"+idUser+"'");
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public boolean setExtAccess(String connectionString, String dataUser,
			String dataPassword,String idUser, int acc) throws RemoteException {
//------GENERO CONNESSIONE AL DATABASE --------		
		
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-----------------------------------------------
		
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	try {
    		command=connection.createStatement();
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	
    
    	
    	try {
    		
    		command.execute("INSERT INTO extuser (numberOfAccess) VALUES"+Integer.toString(acc)+"where idusers='"+idUser+"'");
    		
    	} catch (SQLException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return true;
	}

	public int getExtAccess(String connectionString, String dataUser,
			String dataPassword, String idUser) throws RemoteException {
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	
    	int out=0;
    		try {
    			
	 		data=command.executeQuery("SELECT numberOfAccess  from extuser  WHERE idusers='" +idUser+"'"); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out=out+data.getInt("numberOfAccess");
	 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return out;	
	}

	public int getMaxExtAccess(String connectionString, String dataUser,
			String dataPassword, String iduser) throws RemoteException {
		// TODO Auto-generated method stub
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	
    	int out=0;
    		try {
    			
	 		data=command.executeQuery("SELECT maxAccess  from extuser  WHERE idusers='" +iduser+
	 				"'"); 
	 		
	 		
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out=data.getInt("maxAccess");
	 			}
	 			

	 		
		} 
	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return out;	
	}



	public ArrayList<String> getExtUser(String connectionString, String dataUser,
			String dataPassword) throws RemoteException {
    	Connection connection=null;
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Statement command = null;
    	try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ResultSet data;
    	ArrayList<String> out=new ArrayList<String>();
    		try {
    			
	 		data=command.executeQuery("SELECT idusers FROM users WHERE accessType ='e'"); 
	 		int i=0;
	 		if (data.first()){  //controllo che user non esisti di gia
	 			out.add(data.getString("idusers"));
	 			i=i+1;
	 			}
	 			
	 			while (data.next()){
	 				out.add(data.getString("idusers"));
		 			i=i+1;
	 			}
	 			
 			
 		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
			return out;
	}

	public boolean deleteAllTablesInDatabase(String connectionString,
			String dataUser, String dataPassword) throws RemoteException {
		Connection connection = null;
		Statement command = null;
	
		try {
			connection = DriverManager.getConnection(connectionString,dataUser,dataPassword);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		
		try {
			command=connection.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			
	
			//
			
			
			command.executeUpdate("DELETE FROM programma ");
			command.executeUpdate("DELETE FROM materialiProgetto ");
			command.executeUpdate("DELETE FROM materiali ");
			command.executeUpdate("DELETE FROM personale ");
			command.executeUpdate("DELETE FROM servizi ");
			command.executeUpdate("DELETE FROM voiceOfCost ");
			
			command.executeUpdate("DELETE FROM proposta ");
			command.executeUpdate("DELETE FROM propostaStand ");
			
			command.executeUpdate("DELETE FROM phases ");
			command.executeUpdate("DELETE FROM standProject ");
			command.executeUpdate("DELETE FROM projects ");
			
			command.executeUpdate("DELETE FROM users ");
			command.executeUpdate("DELETE FROM extuser ");
			command.executeUpdate("DELETE FROM customers ");
			
			
			//command.executeUpdate("DELETE FROM order ");
			
			
			

			
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;
	}
}



