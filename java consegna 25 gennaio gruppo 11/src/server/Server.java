package server;

import java.net.MalformedURLException;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;

public class Server {
    @SuppressWarnings("deprecation")
	public static void main(String[] args) throws RemoteException,NotBoundException, AlreadyBoundException{
              
    	Registry r = java.rmi.registry.LocateRegistry.createRegistry(1099);
       
    	try {
			Naming.rebind("LoginInt", new LoginRmi());
			Naming.bind("AdminInt", new AdminRmi());
			Naming.bind("ProjectInt", new ProjectRmi());
			Naming.bind("CustomerInt", new CustomerRmi());
			Naming.bind("PhaseInt", new PhaseRmi());
			Naming.bind("CostInt", new CostRmi());
			Naming.bind("ProposteMaterialiInt", new ProposteMaterialiRmi());
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

            System.out.println("Server is Running");
        
    }
}
