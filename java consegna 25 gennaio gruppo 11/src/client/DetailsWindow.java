package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

import classes.PhaseInterface;
import classes.Project;
import classes.ProjectInterface;
import classes.Proposta;
import classes.PropostaStand;
import classes.ProposteMaterialiInterface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DetailsWindow extends JFrame {
	private JTextField locationtxt;
	private JTextField nomeeventotxt;
	private JTextField linktxt;
	private JTextField metraturatxt;
	private JTextField capienzatxt;
	private JTextField luogotxt;
	private JTextField proptxt;
	private JTextField formatotxt;
	private int _idProject;
	private String _type;
	private JComboBox idpropostastandcombo;
	private JComboBox matcomboBox;
	private JComboBox propcombobox;
	private JComboBox personalecombo;
	private JComboBox servizicombo;
	private JComboBox programmacombo;
	private String _connectionString;
	private String _dataUser;
	private String _dataPwd;
	private String _address;
	

	/**
	 * Launch the application.
	 *//*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DetailsWindow frame = new DetailsWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the frame.
	 * @param string 
	 * @param integer 
	 * @throws SQLException 
	 */
	public DetailsWindow(final Integer idproject, String type, String connectionString, String dataUser, String dataPwd, String address) throws SQLException {
		
		
		_idProject=idproject;
		_type=type;
		_idProject=idproject;
		_address=address;
		_connectionString=connectionString;
		_dataUser=dataUser;
		_dataPwd=dataPwd;
		
		setTitle("Project details");
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Materiale");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(23, 32, 101, 24);
		getContentPane().add(lblNewLabel);
		
		matcomboBox = new JComboBox();
	
		matcomboBox.setFont(new Font("Tahoma", Font.PLAIN, 16));
		matcomboBox.setBounds(118, 34, 101, 24);
		getContentPane().add(matcomboBox);
		
		JLabel lblIdProposta = new JLabel("Id proposta");
		lblIdProposta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIdProposta.setBounds(23, 80, 101, 24);
		getContentPane().add(lblIdProposta);
		
		propcombobox = new JComboBox();
			propcombobox.setModel(new DefaultComboBoxModel(new Integer[] {0}));
		propcombobox.removeAllItems();
		//propcombobox.addItem(3232);
	
		propcombobox.setFont(new Font("Tahoma", Font.PLAIN, 16));
		propcombobox.setBounds(118, 80, 101, 24);
		getContentPane().add(propcombobox);
		
		JLabel lblProposta = new JLabel("Proposta");
		lblProposta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblProposta.setBounds(247, 80, 101, 24);
		getContentPane().add(lblProposta);
		
		JLabel lblFormato = new JLabel("Formato");
		lblFormato.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFormato.setBounds(534, 80, 101, 24);
		getContentPane().add(lblFormato);
		
		JLabel lblLocation = new JLabel("Location");
		lblLocation.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLocation.setBounds(23, 186, 101, 24);
		getContentPane().add(lblLocation);
		
		locationtxt = new JTextField();
		locationtxt.setBounds(93, 188, 112, 24);
		getContentPane().add(locationtxt);
		locationtxt.setColumns(10);
		
		JLabel lblProgramma = new JLabel("Nome Evento");
		lblProgramma.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblProgramma.setBounds(215, 186, 101, 24);
		getContentPane().add(lblProgramma);
		
		nomeeventotxt = new JTextField();
		nomeeventotxt.setColumns(10);
		nomeeventotxt.setBounds(326, 188, 150, 24);
		getContentPane().add(nomeeventotxt);
		
		JLabel lblNumPersonale = new JLabel("Personale");
		lblNumPersonale.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNumPersonale.setBounds(486, 186, 139, 24);
		getContentPane().add(lblNumPersonale);
		
		JLabel lblServizi = new JLabel("Servizi");
		lblServizi.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblServizi.setBounds(23, 241, 101, 24);
		getContentPane().add(lblServizi);
		
		JLabel programma = new JLabel("Programma");
		programma.setFont(new Font("Tahoma", Font.PLAIN, 16));
		programma.setBounds(295, 241, 124, 24);
		getContentPane().add(programma);
		
		JLabel lblIdPropostaStand = new JLabel("Id proposta stand");
		lblIdPropostaStand.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIdPropostaStand.setBounds(23, 399, 138, 24);
		getContentPane().add(lblIdPropostaStand);
		
		idpropostastandcombo = new JComboBox();
		
		idpropostastandcombo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		idpropostastandcombo.setBounds(186, 399, 101, 24);
		getContentPane().add(idpropostastandcombo);
		
		JLabel lblLink = new JLabel("Link");
		lblLink.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLink.setBounds(23, 441, 101, 24);
		getContentPane().add(lblLink);
		
		linktxt = new JTextField();
		linktxt.setColumns(10);
		linktxt.setBounds(93, 443, 644, 24);
		getContentPane().add(linktxt);
		
		JLabel lblMetratura = new JLabel("Metratura");
		lblMetratura.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMetratura.setBounds(23, 483, 101, 24);
		getContentPane().add(lblMetratura);
		
		metraturatxt = new JTextField();
		metraturatxt.setColumns(10);
		metraturatxt.setBounds(93, 485, 144, 24);
		getContentPane().add(metraturatxt);
		
		JLabel lblCapienza = new JLabel("Capienza");
		lblCapienza.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCapienza.setBounds(259, 483, 101, 24);
		getContentPane().add(lblCapienza);
		
		capienzatxt = new JTextField();
		capienzatxt.setColumns(10);
		capienzatxt.setBounds(326, 485, 150, 24);
		getContentPane().add(capienzatxt);
		
		JLabel lblLuogo = new JLabel("Luogo");
		lblLuogo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLuogo.setBounds(534, 483, 101, 24);
		getContentPane().add(lblLuogo);
		
		luogotxt = new JTextField();
		luogotxt.setColumns(10);
		luogotxt.setBounds(587, 483, 150, 24);
		getContentPane().add(luogotxt);
		
		proptxt = new JTextField();
		proptxt.setColumns(10);
		proptxt.setBounds(326, 80, 186, 24);
		getContentPane().add(proptxt);
		
		formatotxt = new JTextField();
		formatotxt.setColumns(10);
		formatotxt.setBounds(620, 82, 150, 24);
		getContentPane().add(formatotxt);
		
		JButton btnNewButton_1 = new JButton("Add materiale progetto");
		
		btnNewButton_1.setBounds(295, 35, 162, 23);
		getContentPane().add(btnNewButton_1);
		
		JButton btnAddPropGrafica = new JButton("Add prop grafica");
	
		btnAddPropGrafica.setBounds(486, 35, 131, 23);
		getContentPane().add(btnAddPropGrafica);
		
		JButton btnAddPropStand = new JButton("Add prop Stand");
		
		btnAddPropStand.setBounds(639, 35, 131, 23);
		getContentPane().add(btnAddPropStand);
		
		personalecombo = new JComboBox();
			personalecombo.setModel(new DefaultComboBoxModel(new String[] {"Personale..."}));
		personalecombo.removeAllItems();
		//personalecombo.addItem(3232);
		personalecombo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		personalecombo.setBounds(587, 186, 150, 24);
		getContentPane().add(personalecombo);
		
		/*	managerCombo.setModel(new DefaultComboBoxModel(new String[] {"Managers..."}));
		managerCombo.removeAllItems();
		managerCombo.addItem(3232);
		*/
		
		servizicombo = new JComboBox();
		servizicombo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		servizicombo.setBounds(93, 241, 150, 24);
		getContentPane().add(servizicombo);
		
		programmacombo = new JComboBox();
		programmacombo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		programmacombo.setBounds(396, 241, 150, 24);
		getContentPane().add(programmacombo);
		
		JButton btnAddServizi = new JButton("Add servizi");
		
		btnAddServizi.setBounds(93, 294, 131, 23);
		getContentPane().add(btnAddServizi);
		
		JButton btnAddPersonale = new JButton("Add personale");
		
		btnAddPersonale.setBounds(288, 294, 131, 23);
		getContentPane().add(btnAddPersonale);
		
		JButton btnAddProgramma = new JButton("Add programma");
		
		btnAddProgramma.setBounds(482, 294, 131, 23);
		getContentPane().add(btnAddProgramma);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 147, 1, 2);
		getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(23, 147, 762, 2);
		getContentPane().add(separator_1);
		
		JButton btnNewButton_2 = new JButton("Exit");
	
		btnNewButton_2.setBounds(358, 552, 89, 23);
		getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton = new JButton("Update");

		btnNewButton.setBounds(481, 552, 89, 23);
		getContentPane().add(btnNewButton);
		
		updatefinestra();
		
		
		matcomboBox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				
				
			}
		});
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (idpropostastandcombo!=null){
				idpropostastandcombo.removeAllItems();
				}
				updateListaPersonale();
				updateListaMaterialeProgetto();
				updateListaProposte();
				updateListaPropStand();
				updateListaProgramma();
				updateListaServizi();
			}
		});
		
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			//add materiale progetto	 idProject, String connectionString, String address, String dataUser, String dataPwd
			AddMaterialeProgetto win = new AddMaterialeProgetto(idproject,_connectionString,_address,_dataUser,_dataPwd);
			win.setVisible(true);
				
				
			}
		});
		btnAddPropGrafica.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//add proposta grafica	 idProject, String connectionString, String address, String dataUser, String dataPwd
				AddOnLine win = new AddOnLine(idproject,_connectionString,_address,_dataUser,_dataPwd);
				win.setVisible(true);								
				}	
							
		});
		
		btnAddPropStand.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!_type.contentEquals("stand")){
					return;
				}
				//add proposta stand	 
				AddStand win = new AddStand(idproject,_connectionString,_address,_dataUser,_dataPwd);
				win.setVisible(true);								
				}
				
			
		});
		btnAddServizi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				if (!_type.contentEquals("stand")){
					return;
				}
				//add servizi
				AddServizio win = new AddServizio(idproject,_connectionString,_address,_dataUser,_dataPwd);
				win.setVisible(true);
			}
		});
		btnAddPersonale.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!_type.contentEquals("stand")){
					return;
				}
				//add personale
				AddPersonale win = new AddPersonale(idproject,_connectionString,_address,_dataUser,_dataPwd);
				win.setVisible(true);
			}
		});
		btnAddProgramma.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (!_type.contentEquals("stand")){
					return;
				}
				//add programma
				AddProgramma win = new AddProgramma(idproject,_connectionString,_address,_dataUser,_dataPwd);
				win.setVisible(true);
			}
		});
		propcombobox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					
					Proposta proposta = e.getproposta(_connectionString, _dataUser, _dataPwd, _idProject, (Integer)propcombobox.getSelectedItem());
					if (proposta!=null){
					proptxt.setText(proposta.getProposta());
					formatotxt.setText(proposta.getFormato());
					}
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			
				
				
				
			}
		});
		
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				linktxt.setText("");
				metraturatxt.setText("");
				capienzatxt.setText("");
				luogotxt.setText("");
				
				closewindow();
			}

			
		});
		
		idpropostastandcombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				
				if (!_type.contentEquals("stand")){
					return;
				}
				
				//PropostaStand prop=null;
				try {
					PropostaStand prop=e.getpropostaStand(_connectionString, _dataUser, _dataPwd, idproject, (Integer)idpropostastandcombo.getSelectedItem());
					if (prop!=null){
						linktxt.setText(prop.getLink());
						metraturatxt.setText(prop.getMetratura());
						capienzatxt.setText(prop.getCapienza());
						luogotxt.setText(prop.getLuogo());
					}
					else{
						linktxt.setText("");
						metraturatxt.setText("");
						capienzatxt.setText("");
						luogotxt.setText("");
					}
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				
			}
		});
		
		
		//aggiorno combo box
		updateListaPersonale();
		updateListaMaterialeProgetto();
		updateListaProposte();
		updateListaPropStand();
		updateListaProgramma();
		updateListaServizi();
	}
	
	private void updatefinestra() throws SQLException {
		// TODO Auto-generated method stub
		
		ProjectInterface e = null;
		
		try {
			System.out.println("sono in detailswindow address ="+_address);
			e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//Project project = null;
		if (_type.contentEquals("stand")){
			
			try {
				Project project = e.getProject(_connectionString, _dataUser, _dataPwd, _idProject);
				String location=project.get_location();
				String nome=project.get_nome();
				locationtxt.setText(location);
				nomeeventotxt.setText(nome);
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
		}
		else{
			locationtxt.setText("");
			nomeeventotxt.setText("");
		}
		
	}

	public void updateListaMaterialeProgetto(){
		matcomboBox.removeAllItems();
		ProposteMaterialiInterface e = null;
		ArrayList<Integer> ids=new ArrayList<Integer>();
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			ids=e.getListaMatprogetto(_connectionString, _dataUser, _dataPwd, _idProject);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//riempiio di item la combobox
		
		for (int i=0; i<ids.size(); i=i+1){
			matcomboBox.addItem(ids.get(i));
		}
		
	}
	
	public void updateListaProposte(){
		
		propcombobox.removeAll();
		ProposteMaterialiInterface e = null;
		ArrayList<Integer> ids=new ArrayList<Integer>();
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			ids=e.getListaProposte(_connectionString, _dataUser, _dataPwd, _idProject);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//riempiio di item la combobox
		
		for (int i=0; i<ids.size(); i=i+1){
			propcombobox.addItem(ids.get(i));
		}
		
	}
	
	public void updateListaPropStand(){
		idpropostastandcombo.removeAll();
		ProposteMaterialiInterface e = null;
		ArrayList<Integer> ids=new ArrayList<Integer>();
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			ids=e.getListaProposteStand(_connectionString, _dataUser, _dataPwd, _idProject);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//riempiio di item la combobox
		
		for (int i=0; i<ids.size(); i=i+1){
			idpropostastandcombo.addItem(ids.get(i));
		}
	}
	
	public void updateListaProgramma(){
		programmacombo.removeAllItems();
		ProposteMaterialiInterface e = null;
		ArrayList<String> ids=new ArrayList<String>();
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			ids=e.getProgramma(_connectionString, _dataUser, _dataPwd, _idProject);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//riempiio di item la combobox
		
		for (int i=0; i<ids.size(); i=i+1){
			programmacombo.addItem(ids.get(i) );
		}
	}
	public void updateListaServizi(){
		
		servizicombo.removeAllItems();
		ProposteMaterialiInterface e = null;
		ArrayList<String> ids=new ArrayList<String>();
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			ids=e.getServizi(_connectionString, _dataUser, _dataPwd, _idProject);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//riempiio di item la combobox
		
		for (int i=0; i<ids.size(); i=i+1){
			servizicombo.addItem(ids.get(i));
		}
		
	}
	public void updateListaPersonale(){
		if (personalecombo!=null){
		personalecombo.removeAllItems();
		}
		ProposteMaterialiInterface e = null;
		ArrayList<String> ids=new ArrayList<String>();
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			ids=e.getPersonale(_connectionString, _dataUser, _dataPwd, _idProject);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//riempiio di item la combobox
		
		for (int i=0; i<ids.size(); i=i+1){
			 personalecombo.addItem(ids.get(i));
		}
}
	
	private void closewindow() {
		// TODO Auto-generated method stub
		this.setVisible(false);
	}
}

