package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;

import classes.ProposteMaterialiInterface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddPersonale extends JFrame {

	private JPanel contentPane;
	private JTextField idpersonaletxt;
	private JTextField ruolotxt;

	private int _idProject;
	private String _connectionString;
	private String _address;
	private String _dataUser;
	private String _dataPwd;
	private JLabel label;
	private JComboBox comboBox;
	private JButton btnDelete;
	private JButton btnExit;
	
	public AddPersonale(int idProject, String connectionString, String address, String dataUser, String dataPwd) {
		_idProject=idProject;
		_connectionString=connectionString;
		_address=address;
		_dataUser=dataUser;
		_dataPwd=dataPwd;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 601, 321);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Id personale");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(24, 26, 124, 24);
		contentPane.add(lblNewLabel);
		
		idpersonaletxt = new JTextField();
		idpersonaletxt.setBounds(143, 26, 200, 24);
		contentPane.add(idpersonaletxt);
		idpersonaletxt.setColumns(10);
		
		JLabel lblRuolo = new JLabel("Ruolo");
		lblRuolo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblRuolo.setBounds(24, 78, 124, 24);
		contentPane.add(lblRuolo);
		
		ruolotxt = new JTextField();
		ruolotxt.setColumns(10);
		ruolotxt.setBounds(143, 78, 200, 24);
		contentPane.add(ruolotxt);
		
		JButton btnNewButton = new JButton("Add");
		
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(205, 123, 138, 23);
		contentPane.add(btnNewButton);
		
		label = new JLabel("Id personale");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(24, 181, 124, 24);
		contentPane.add(label);
		
		comboBox = new JComboBox();
		comboBox.setBounds(143, 183, 200, 24);
		contentPane.add(comboBox);
		
		btnDelete = new JButton("Delete");
	
		btnDelete.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnDelete.setBounds(373, 182, 138, 23);
		contentPane.add(btnDelete);
		
		btnExit = new JButton("Exit");
	
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnExit.setBounds(373, 249, 138, 23);
		contentPane.add(btnExit);
		
		updatecombobox();
		
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
		ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				try {
					e.deletePersonale(_connectionString, _dataUser, _dataPwd, _idProject,Integer.valueOf((String)comboBox.getSelectedItem()) );
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatecombobox();
			}
		});
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (idpersonaletxt.getText().contentEquals("")||ruolotxt.getText().contentEquals("")){
					return;
				}
				
				
				
				ProposteMaterialiInterface e = null;
				
				
				try {
					e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				
				try {
					e.addPersonale(_connectionString, _dataUser, _dataPwd, _idProject,Integer.valueOf(idpersonaletxt.getText()) , ruolotxt.getText());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatecombobox();
				
			}
		});
		
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				closewin();
			}
		});
		
	}
	protected void closewin() {
		// TODO Auto-generated method stub
		updatecombobox();
		this.setVisible(false);
	}
	private void updatecombobox(){
		
		comboBox.removeAllItems();
		ArrayList<String> lista = new ArrayList<String>();
		ProposteMaterialiInterface e = null;
		
		
		try {
			e = (ProposteMaterialiInterface)Naming.lookup(_address+"/ProposteMaterialiInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			lista= e.getPersonale(_connectionString, _dataUser, _dataPwd, _idProject);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for (int i=0; i<lista.size(); i=i+1){
			
			comboBox.addItem(lista.get(i));
		}
	}
}
