package client;
import java.sql.*;
import java.awt.BorderLayout;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;













import classes.About;
import classes.AdminInterface;
import classes.CustomerInterface;
import classes.LoginInterface;
import classes.ProjectInterface;



public class MainWindow extends JFrame {//throws NotBoundException,MalformedURLException,RemoteException {

	
	private  JButton btnLogout;
	private  JButton btnLogin;
	private  JButton btnAddUser;
	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private JTextField textField_1;
	//private Login log;
	private AddUserWindow addUserWindow;
	private ProjectWindow projectWindow;
	
	private String _user=null;
	private String _pwd=null;
	private String _accessType=null;
	private String _address;
	private String _dataHost=null;
	private String _dataName=null;
	private String _dataUser=null;
	private String _dataPassword=null;
	private JButton btnManageOrder;
	//private Connection _connection;
	

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	//public MainWindow(final Login log) {
	public MainWindow(LoginInterface c_, String address,  final String dataHost,final String dataName,final String dataUser,final String dataPassword) {
		
		//_connection=connection;
		_accessType=null;
		_address=address;
		_dataHost=dataHost;
		_dataName=dataName;
		_dataUser=dataUser;
		_dataPassword = dataPassword;
		final LoginInterface c;
		c=c_;
		setTitle("Publicity");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 974, 426);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240, 240, 240));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(55, 62, 99, 24);
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(lblUsername);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		
		
	
		textField.setBounds(183, 62, 254, 24);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(55, 119, 99, 24);
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		
		passwordField.setBounds(183, 121, 254, 24);
		contentPane.add(passwordField);
		
		
		btnLogin = new JButton("Login");
		
		btnLogin.setBounds(495, 62, 135, 24);
		btnLogin.setEnabled(false);
		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(btnLogin);
		
		JLabel lblLogin = new JLabel("Hai effettuato il Login come");
		lblLogin.setBounds(55, 178, 206, 24);
		lblLogin.setEnabled(false);
		lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(lblLogin);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(55, 213, 886, 2);
		contentPane.add(separator);
		
		btnLogout = new JButton("Logout");
		
		btnLogout.setEnabled(false);
		btnLogout.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnLogout.setBounds(495, 119, 135, 24);
		contentPane.add(btnLogout);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField_1.setBounds(271, 178, 166, 24);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		final JButton projectButton = new JButton("Manage Projects");
	
		projectButton.setEnabled(false);
		projectButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		projectButton.setBounds(55, 245, 166, 24);
		contentPane.add(projectButton);
		
		final JButton btnCustomer = new JButton("Manage Customers");

		btnCustomer.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnCustomer.setEnabled(false);
		btnCustomer.setBounds(271, 245, 166, 24);
		contentPane.add(btnCustomer);
		
		btnAddUser = new JButton("Add User");
		
		btnAddUser.setEnabled(false);
		btnAddUser.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnAddUser.setBounds(495, 178, 135, 24);
		contentPane.add(btnAddUser);
		
		JButton btnAbout = new JButton("About");
		btnAbout.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				About winAbout = new About();
				winAbout.setVisible(true);
			}
		});
		btnAbout.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnAbout.setBounds(10, 352, 99, 24);
		contentPane.add(btnAbout);
		
		btnManageOrder = new JButton("Manage Order");
	
		btnManageOrder.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnManageOrder.setBounds(464, 245, 166, 24);
		contentPane.add(btnManageOrder);
		
		JButton btnDeleteDatabaseContent = new JButton("Delete DataBase Content");
	
		btnDeleteDatabaseContent.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnDeleteDatabaseContent.setBounds(133, 308, 227, 24);
		contentPane.add(btnDeleteDatabaseContent);
		
		JButton btnDatabasePopulator = new JButton(" DataBase Populator");
		btnDatabasePopulator.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					DatabasePopulator.populate(_address, c.getConnectString(_dataHost, _dataName), _dataUser, _dataPassword);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnDatabasePopulator.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnDatabasePopulator.setBounds(370, 308, 227, 24);
		contentPane.add(btnDatabasePopulator);
		
		/*----------------------------------------*/
		try {
			String connectionString=c.getConnectString(_dataHost, _dataName);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			
			addUserWindow = new AddUserWindow(_address,c.getConnectString(_dataHost, _dataName),_dataUser,_dataPassword, _dataHost, _dataName);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		addUserWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addUserWindow.setVisible(false);
		
	
		
		
		
		/*_________________________________________*/	
		
		btnManageOrder.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				if (!_accessType.contentEquals("m")||_accessType.contentEquals("d")){
					return;
				}
				
				OrderWin orderwin;
				try {
					orderwin = new OrderWin(c.getConnectString(_dataHost, _dataName), _address, _dataUser, _dataPassword);
					orderwin.setVisible(true);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		btnDeleteDatabaseContent.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					DatabasePopulator.deleteDatabaseContent(_address, c.getConnectString(_dataHost, _dataName), _dataUser, _dataPassword);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		projectButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//manage project button
			
				projectWindow = new ProjectWindow(_address, _accessType, _dataHost, _dataName, _dataPassword, _dataUser);
				projectWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				projectWindow.setVisible(true);
				
				
			}
		});
		
		btnCustomer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			if (_accessType.contentEquals("m")|| _accessType.contentEquals("s")){	
				//mostro finestra managecustomer
				String connectionString=SqlConnect.getConnectString(_dataHost, _dataName);	
			AddCustomer addCustomer=new AddCustomer(_address, connectionString, dataUser, dataPassword);	
			addCustomer.setVisible(true);
			}
			}
				
		});
		
		
		/* add user */
		
		btnAddUser.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				showAddUserWindow();
				
			}
		});
		
		
		
		btnLogin.addMouseListener(new MouseAdapter() {
			
			//GESTIONE CLICK SU LOGIN (PROBLEMA CON RMI)
			@SuppressWarnings({ "null", "unused" })
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				textField.setEnabled(false);
				passwordField.setEnabled(false);
				btnLogin.setEnabled(false);
				btnLogout.setEnabled(true);
				
				/* login*/
				
				try {
					
					//ProvaRmiInterface c = (ProvaRmiInterface)Naming.lookup("rmi://localhost:1099/ProvaClassePerRmi");
		           // System.out.println("sto per chiamare setlogin");
		            
		            //ArrayList<String> pwdAccess=null;
		            String pwd=null;
		            String access=null;
		            String connectionString=SqlConnect.getConnectString(_dataHost, _dataName);
		            System.out.println("interfaccia chiamata... +connectionString "+connectionString);
		            //Connection connection;
		            //connection = DriverManager.getConnection(connectionString,_dataUser,_dataPassword);
		            
		         
		          
		          
					pwd=c.setLoginFromData(connectionString,_dataUser, _dataPassword, textField.getText(), passwordField.getPassword(),0);
		         
					System.out.println("password recuperata...");
					
					access =c.setLoginFromData(connectionString,_dataUser, _dataPassword, textField.getText(), passwordField.getPassword(),1);
					System.out.println("access recuperata...");
		         
		            
					//System.out.println("set login chiamato pwd= "+pwd);
		            if (pwd!=null){
		            _user=textField.getText();
		            _pwd=pwd;
		            _accessType=access;
		            }
		            else
		            {
		            	_user=null;
			            _pwd=null;
			            _accessType=null;
		            }
		            
		            
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				
				
				/*controllo il login*/
				
	
					try {
					
						//ProvaRmiInterface c = (ProvaRmiInterface)Naming.lookup("rmi://localhost:1099/Login");
		            System.out.println("Client is connected to server accessType = "+_accessType);
		            
		            if (_accessType==null){//||(!_accessType.matches("a"))||(!_accessType.matches("m"))||(!_accessType.matches("d"))||(!_accessType.matches("s"))||(!_accessType.matches("e"))){
					textField_1.setText("Error");
					textField.setText("");
					textField.setEnabled(true);
					passwordField.setText("");
					passwordField.setEnabled(true);
					btnLogin.setEnabled(false);
					btnLogout.setEnabled(false);
					return;
				}
				
		
				//System.out.println("dati login presi... " +_accessType);
				if (_accessType.matches("a")){
					textField_1.setText("Admin");
					btnAddUser.setEnabled(true);
					
					
				}
				if (_accessType.matches("m")){
					textField_1.setText("Manager");
					projectButton.setEnabled(true);
					btnCustomer.setEnabled(true);
					
				}
				if (_accessType.matches("d")){
					textField_1.setText("Dipendente");
					projectButton.setEnabled(true);
					
					
				}
				if (_accessType.matches("s")){
					textField_1.setText("Super");
					projectButton.setEnabled(true);
					btnCustomer.setEnabled(true);
					btnAddUser.setEnabled(true);
					
				}
				if (_accessType.matches("e")){
					textField_1.setText("Esterno");
					boolean b=false;
					b=accessIncrement();
					if(!b){
						textField_1.setText("ERROR: you ve reached the max number of access!");
					}
				}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
		
				
			}
		});
		
		// fine controllo LOGIN
		
		passwordField.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				
			
				enableLogIn();
				
			}
		});
		
		textField.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				
				
				enableLogIn();
			}
		});
		
		btnLogout.addMouseListener(new MouseAdapter() {
			
			/* GESTIONE DEL CLICK SU LOGOUT */
			@Override
			public void mouseClicked(MouseEvent arg0) {
				textField_1.setText("");
				
				try {
					//ProvaRmiInterface c = (ProvaRmiInterface)Naming.lookup("rmi://localhost:1099/Login");
		            //System.out.println("Client is connected to server");
		            //System.out.println(c.add(10, 10));
					//c.setLogin(null, null);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*log.set_accessType(null);*/ // ??????????????????????????????????????????????????????????????
				addUserWindow.setVisible(false);
				btnAddUser.setEnabled(false);
				textField.setText("");
				textField.setEnabled(true);
				passwordField.setText("");
				passwordField.setEnabled(true);
				btnLogin.setEnabled(false);
				btnLogout.setEnabled(false);
				projectButton.setEnabled(false);
				
				
			}
		});
		
		
		}
		
		
		protected boolean accessIncrement() {
		// TODO Auto-generated method stub
			AdminInterface f = null;
			
			
			try {
				f = (AdminInterface)Naming.lookup(_address+"/AdminInt");
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NotBoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			int currentAccess=0;
			try {
				currentAccess = f.getExtAccess(SqlConnect.getConnectString(_dataHost, _dataName), _dataUser, _dataPassword, textField.getText());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int maxAccess=0;
			try {
				maxAccess=f.getMaxExtAccess(SqlConnect.getConnectString(_dataHost, _dataName), _dataUser, _dataPassword, textField.getText());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (currentAccess>=maxAccess){
				return false;
			}
			
			else {
				try {
					f.setExtAccess(SqlConnect.getConnectString(_dataHost, _dataName), _dataUser, _dataPassword, textField.getText(),currentAccess+1);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return true;
			
			
	}


		//fine costruttore
	
	public boolean dataLogOk(){
		
		String userText=textField.getText();
		if (!textField.getText().isEmpty()&& !userText.contains(";")&& !userText.contains("\\") ){
			
			
		
		char[] data= passwordField.getPassword();
		String pwdText = String.valueOf(data);
		if (!pwdText.isEmpty()&& !pwdText.contains(";")&& !pwdText.contains("\\")){
			
			
			return (true);
		}
	}
	return (false);
		
	}
	
	public void enableLogIn(){
		
		
		btnLogin.setEnabled(dataLogOk());
		
	}
	
	public void showAddUserWindow() {
		
		addUserWindow.setVisible(true);
		
		
		
	}
	}


	
	
	

