package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import classes.AdminInterface;
import classes.Customer;
import classes.CustomerInterface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class AddCustomer extends JFrame {

	private JPanel contentPane;
	private JTextField idCustomertxt;
	private JTextField celltxt;
	private JTextField mailtxt;
	private JTextField referencetxt;
	private String _address;
	private String _connectionString;
	private String _dataUser;
	private String _dataPassword;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddCustomer frame = new AddCustomer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the frame.
	 * @param connectionString 
	 * @param _accessType 
	 */
	public AddCustomer(String address, String connectionString, String dataUser, String dataPassword) {
		_address=address;
		_connectionString=connectionString;
		_dataUser=dataUser;
		_dataPassword=dataPassword;
		setTitle("Add Customer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 784, 250);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblName.setBounds(28, 33, 124, 24);
		contentPane.add(lblName);
		
		idCustomertxt = new JTextField();
		idCustomertxt.setColumns(10);
		idCustomertxt.setBounds(147, 35, 200, 24);
		contentPane.add(idCustomertxt);
		
		JLabel lblCell = new JLabel("Cell");
		lblCell.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCell.setBounds(28, 96, 124, 24);
		contentPane.add(lblCell);
		
		celltxt = new JTextField();
		celltxt.setColumns(10);
		celltxt.setBounds(147, 96, 200, 24);
		contentPane.add(celltxt);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMail.setBounds(392, 96, 124, 24);
		contentPane.add(lblMail);
		
		mailtxt = new JTextField();
		mailtxt.setColumns(10);
		mailtxt.setBounds(511, 96, 200, 24);
		contentPane.add(mailtxt);
		
		JLabel lblReference = new JLabel("Reference");
		lblReference.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblReference.setBounds(376, 33, 124, 24);
		contentPane.add(lblReference);
		
		referencetxt = new JTextField();
		referencetxt.setColumns(10);
		referencetxt.setBounds(511, 35, 200, 24);
		contentPane.add(referencetxt);
		
		JButton btnNewButton = new JButton("Add");
	
		btnNewButton.setBounds(28, 159, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Delete");

		btnNewButton_1.setBounds(147, 159, 89, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Exit");

		btnNewButton_2.setBounds(270, 159, 89, 23);
		contentPane.add(btnNewButton_2);
		
		
		//**************** EVENTI *************
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				//int idCustomer, String contact, String mail, String cell
				//Customer customer= new Customer("", "", "", "");
			Customer customer=new Customer(idCustomertxt.getText() , referencetxt.getText(), mailtxt.getText(), celltxt.getText());	
				//Aggiungo customer
				CustomerInterface e = null;
				try {
					e = (CustomerInterface)Naming.lookup(_address+"/CustomerInt");
					e.addCustomer(_connectionString, _dataUser, _dataPassword, customer);
					System.out.println("customer aggiunto da interfaccia");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				//elimino customer
				
				CustomerInterface e = null;
				try {
					e = (CustomerInterface)Naming.lookup(_address+"/CustomerInt");
					e.deleteCustomer(_connectionString, _dataUser, _dataPassword, idCustomertxt.getText());
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				//exit
				closeWindow();
				
			}
		});
		
		
		
	}

	protected void closeWindow() {
		// TODO Auto-generated method stub
		this.setVisible(false);
	}
}
