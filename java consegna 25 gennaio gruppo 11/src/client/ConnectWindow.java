package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ConnectWindow extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private boolean ok;
	private String address;
	private JTextField sqlUser;
	private JTextField sqlPwd;
	private JTextField dataServer;
	private JTextField dataName;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConnectWindow frame = new ConnectWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConnectWindow() {
		setOk(false);
		setAddress("localhost");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 784, 245);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Remote Server");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(26, 11, 114, 24);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField.setBounds(153, 13, 288, 24);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Connect");

		btnNewButton.setBounds(566, 13, 116, 24);
		contentPane.add(btnNewButton);
		
		JLabel lblSqlUser = new JLabel("Sql User");
		lblSqlUser.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSqlUser.setBounds(26, 56, 80, 24);
		contentPane.add(lblSqlUser);
		
		sqlUser = new JTextField();
		sqlUser.setFont(new Font("Tahoma", Font.PLAIN, 16));
		sqlUser.setBounds(172, 56, 161, 24);
		contentPane.add(sqlUser);
		sqlUser.setColumns(10);
		
		JLabel lblSqlPwd = new JLabel("Sql Pwd");
		lblSqlPwd.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSqlPwd.setBounds(354, 56, 80, 24);
		contentPane.add(lblSqlPwd);
		
		sqlPwd = new JTextField();
		sqlPwd.setFont(new Font("Tahoma", Font.PLAIN, 16));
		sqlPwd.setColumns(10);
		sqlPwd.setBounds(482, 56, 200, 24);
		contentPane.add(sqlPwd);
		
		JLabel lblDatabaseServer = new JLabel("DataBase server");
		lblDatabaseServer.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDatabaseServer.setBounds(26, 109, 136, 24);
		contentPane.add(lblDatabaseServer);
		
		dataServer = new JTextField();
		dataServer.setFont(new Font("Tahoma", Font.PLAIN, 16));
		dataServer.setColumns(10);
		dataServer.setBounds(172, 109, 167, 24);
		contentPane.add(dataServer);
		
		JLabel lblDatabaseName = new JLabel("Database name");
		lblDatabaseName.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDatabaseName.setBounds(354, 109, 116, 24);
		contentPane.add(lblDatabaseName);
		
		dataName = new JTextField();
		dataName.setFont(new Font("Tahoma", Font.PLAIN, 16));
		dataName.setColumns(10);
		dataName.setBounds(482, 109, 200, 24);
		contentPane.add(dataName);
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			
			//gestione click su connetti
			@Override
			public void mouseClicked(MouseEvent arg0) {
			String string= null;//"localhost";	
			if (textField.getText().contentEquals("")){
				string="localhost";
			}
			else
			{
				string=textField.getText();
			}
			
			
			setAddress(("rmi://"+string+":1099"));   //   /LoginInt"));	
			setOk(true);	
				
			}
		});
				
	}

	public boolean isOk() {
		return ok;
	}

	public String getUserSql(){
		String out= sqlUser.getText();
		return out;
	}
	public String getPwdSql(){
		String out= sqlPwd.getText();
		return out;
	}
	public String getDataNameSql(){
		String out= dataName.getText();
		return out;
	}
	public String getHostSql(){
		String out= dataServer.getText();
		return out;
	}
	
	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
