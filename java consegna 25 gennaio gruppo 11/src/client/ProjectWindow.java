package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.Font;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

//import server.OrderInterface_;








import javax.swing.MutableComboBoxModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultComboBoxModel;


import classes.AdminInterface;
import classes.CostInterface;
import classes.Customer;
import classes.CustomerInterface;
import classes.Phase;
import classes.PhaseInterface;
import classes.Project;
import classes.ProjectInterface;
import classes.StandProject;
import classes.VoiceOfcost;

import com.mysql.fabric.xmlrpc.base.Data;

import java.awt.List;

public class ProjectWindow extends JFrame {
	


	private JPanel contentPane;
	private JTextField customertxt;
	private JTextField deadlinetxt;
	private JTextField coststxt;
	private JTextField statustxt;
	private JTextField typetxt;
	private JTextField managertxt;
	private JTextField deadfasetxt;
	private JTextField totcosttxt;
	private JTextField materialcosttxt;
	private JTextField employeecosttxt;
	private JTextField othercosttxt;
	private JTextField typecosttxt;
	private JTextField amounttxt;
	private JTextField loantxt;
	private String _address;
	private String _accessType;
	//private int[] _listIdProjects;
	private JComboBox projectcombo;
	private JComboBox phasecombo;
	private JComboBox costcombo;
	private JComboBox fasefinishedcombo;
	private String _connectionString;
	private String _dataUser;
	private String _dataPwd;
	private JTextField idnewprojecttxt;
	private JTextField idnewphasetxt;
	private JTextField fasefinishedtxt;
	private JTextField emptxt;
	private JComboBox managerCombo;

	private JComboBox customerCombo;
	//private JComboBox costCombo;
	private JComboBox empcombo;
	private JTextField nameProjecttxt;
	private JTextField creationdatetxt;
	private JTextField standnametxt;
	private JTextField standlocationtxt;
	private JTextField desctxt;
	private JButton btnRefresh;
	private JButton showbutton;

	
	
	
	

	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProjectWindow frame = new ProjectWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	/**
	 * Create the frame.
	 */
	public ProjectWindow(String address, String accessType, String dataHost, String dataName, String dataPwd, String dataUser) {
		//_listIdProjects=new int[]();
		_address=address;
		_accessType=accessType;
		String connectionString=SqlConnect.getConnectString(dataHost, dataName);
		_connectionString= connectionString;
		_dataPwd=dataPwd;
		_dataUser=dataUser;
		/*
		managerString= new ArrayList<String>();
		list=new List();
		*/
		
		setTitle("Projects Management");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 890, 778);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btn1 = new JButton("Create Project");

		btn1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btn1.setBounds(330, 21, 144, 24);
		contentPane.add(btn1);
		
		final JButton btn3 = new JButton("Delete Project");
		btn3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btn3.setBounds(363, 82, 145, 24);
		contentPane.add(btn3);
		
		JButton btn2 = new JButton("Close Project");

		btn2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btn2.setBounds(224, 82, 129, 24);
		contentPane.add(btn2);
		
		managerCombo=  new JComboBox();
				//final JComboBox managerCombo = new JComboBox();
		managerCombo.setModel(new DefaultComboBoxModel(new String[] {"Managers..."}));
		managerCombo.removeAllItems();
		managerCombo.addItem(3232);

		managerCombo.setToolTipText("");
		managerCombo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		managerCombo.setBounds(610, 222, 135, 24);
		contentPane.add(managerCombo);
		
		updateManagerComboBox();
		
	
		
		projectcombo = new JComboBox();
		projectcombo.setModel(new DefaultComboBoxModel(new Integer[] {0}));
		projectcombo.removeAllItems();
		projectcombo.addItem(3232);

		projectcombo.setToolTipText("");
		projectcombo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		projectcombo.setBounds(132, 82, 82, 24);
		contentPane.add(projectcombo);


		updateProjects();
		
			//final 
		 phasecombo = new JComboBox();
		 phasecombo.addMouseListener(new MouseAdapter() {
		 	@Override
		 	public void mouseClicked(MouseEvent arg0) {
		 		showbutton .setEnabled(true);
		 	}
		 });
		phasecombo.setModel(new DefaultComboBoxModel(new Integer[] {0}));
		phasecombo.removeAllItems();
		phasecombo.addItem("Select...");
		
		phasecombo.setToolTipText("");
		phasecombo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		phasecombo.setBounds(132, 362, 129, 24);
		contentPane.add(phasecombo);
		//updatephaselist();
		
		
		JLabel lblSelectProject = new JLabel("Select Project");
		lblSelectProject.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSelectProject.setBounds(24, 82, 123, 24);
		contentPane.add(lblSelectProject);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(24, 56, 721, 9);
		contentPane.add(separator);
		
		JLabel lblNewLabel = new JLabel("Select Phase");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(24, 360, 98, 24);
		contentPane.add(lblNewLabel);
		
		
		
		JLabel lblNewLabel_1 = new JLabel("Customer");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(24, 147, 82, 24);
		contentPane.add(lblNewLabel_1);
		
		customertxt = new JTextField();
		customertxt.setBounds(128, 149, 135, 24);
		contentPane.add(customertxt);
		customertxt.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Deadline");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_2.setBounds(275, 147, 77, 24);
		contentPane.add(lblNewLabel_2);
		
		deadlinetxt = new JTextField();
		deadlinetxt.setColumns(10);
		deadlinetxt.setBounds(362, 149, 135, 24);
		contentPane.add(deadlinetxt);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(24, 117, 721, 2);
		contentPane.add(separator_1);
		
		JLabel lblTotCosts = new JLabel("Tot Costs");
		lblTotCosts.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTotCosts.setBounds(523, 117, 77, 24);
		contentPane.add(lblTotCosts);
		
		coststxt = new JTextField();
		coststxt.setColumns(10);
		coststxt.setBounds(610, 117, 135, 24);
		contentPane.add(coststxt);
		
		JLabel lblStatus = new JLabel("Status");
		lblStatus.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblStatus.setBounds(24, 196, 82, 24);
		contentPane.add(lblStatus);
		
		statustxt = new JTextField();
		statustxt.setColumns(10);
		statustxt.setBounds(128, 198, 135, 24);
		contentPane.add(statustxt);
		
		JLabel lblType = new JLabel("Type");
		lblType.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblType.setBounds(275, 196, 82, 24);
		contentPane.add(lblType);
		
		typetxt = new JTextField();
		typetxt.setColumns(10);
		typetxt.setBounds(362, 198, 135, 24);
		contentPane.add(typetxt);
		
		JLabel lblManager = new JLabel("Manager");
		lblManager.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblManager.setBounds(523, 196, 82, 24);
		contentPane.add(lblManager);
		
		managertxt = new JTextField();
		managertxt.setColumns(10);
		managertxt.setBounds(610, 198, 135, 24);
		contentPane.add(managertxt);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(24, 312, 721, 2);
		contentPane.add(separator_2);
		
		JLabel lblEmployee = new JLabel("Employee");
		lblEmployee.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblEmployee.setBounds(46, 395, 82, 24);
		contentPane.add(lblEmployee);
		
		JLabel label = new JLabel("Deadline");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(275, 422, 77, 24);
		contentPane.add(label);
		
		deadfasetxt = new JTextField();
		deadfasetxt.setColumns(10);
		deadfasetxt.setBounds(362, 422, 135, 24);
		contentPane.add(deadfasetxt);
		
		JLabel label_1 = new JLabel("Tot Costs");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_1.setBounds(523, 422, 77, 24);
		contentPane.add(label_1);
		
		totcosttxt = new JTextField();
		totcosttxt.setEditable(false);
		totcosttxt.setColumns(10);
		totcosttxt.setBounds(622, 422, 123, 24);
		contentPane.add(totcosttxt);
		
		JLabel lblCostMaterials = new JLabel("Cost Materials");
		lblCostMaterials.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCostMaterials.setBounds(507, 149, 109, 24);
		contentPane.add(lblCostMaterials);
		
		materialcosttxt = new JTextField();
		materialcosttxt.setColumns(10);
		materialcosttxt.setBounds(610, 149, 133, 24);
		contentPane.add(materialcosttxt);
		
		JLabel lblCostEmployee = new JLabel("Cost Employees");
		lblCostEmployee.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCostEmployee.setBounds(273, 457, 116, 24);
		contentPane.add(lblCostEmployee);
		
		employeecosttxt = new JTextField();
		employeecosttxt.setEditable(false);
		employeecosttxt.setColumns(10);
		employeecosttxt.setBounds(399, 459, 98, 24);
		contentPane.add(employeecosttxt);
		
		JButton btnShowDeatils = new JButton("Show Deatils");
	
		btnShowDeatils.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnShowDeatils.setBounds(128, 249, 135, 24);
		contentPane.add(btnShowDeatils);
		
		JLabel lblOtherCosts = new JLabel("Other Costs");
		lblOtherCosts.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblOtherCosts.setBounds(523, 457, 106, 24);
		contentPane.add(lblOtherCosts);
		
		othercosttxt = new JTextField();
		othercosttxt.setEditable(false);
		othercosttxt.setColumns(10);
		othercosttxt.setBounds(622, 459, 123, 24);
		contentPane.add(othercosttxt);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(24, 492, 727, 2);
		contentPane.add(separator_3);
		
		JButton btn5 = new JButton("Delete Phase");
	
		btn5.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btn5.setBounds(319, 360, 145, 24);
		contentPane.add(btn5);
		
		JLabel lblSelectVoiceOf = new JLabel(" Voice of Cost");
		lblSelectVoiceOf.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSelectVoiceOf.setBounds(24, 562, 123, 24);
		contentPane.add(lblSelectVoiceOf);
		
		//final 
		costcombo = new JComboBox();
			costcombo.setModel(new DefaultComboBoxModel(new Integer[] {0}));
		costcombo.removeAllItems();
		costcombo.setBounds(157, 564, 145, 24);
		contentPane.add(costcombo);
		
		JLabel lblTypeOfCost = new JLabel("Type of Cost");
		lblTypeOfCost.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTypeOfCost.setBounds(19, 607, 109, 24);
		contentPane.add(lblTypeOfCost);
		
		typecosttxt = new JTextField();
		typecosttxt.setColumns(10);
		typecosttxt.setBounds(138, 609, 123, 24);
		contentPane.add(typecosttxt);
		
		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAmount.setBounds(270, 607, 82, 24);
		contentPane.add(lblAmount);
		
		amounttxt = new JTextField();
		amounttxt.setColumns(10);
		amounttxt.setBounds(341, 609, 64, 24);
		contentPane.add(amounttxt);
		
		JButton btn6 = new JButton("Delete Cost");
	
		btn6.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btn6.setBounds(329, 562, 145, 24);
		contentPane.add(btn6);
		
		JButton btn4 = new JButton("Create Phase");

		btn4.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btn4.setBounds(319, 325, 144, 24);
		contentPane.add(btn4);
		
		JButton btnNewVoiceOf = new JButton("New Voice Of Cost");
	
		btnNewVoiceOf.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewVoiceOf.setBounds(24, 517, 167, 24);
		contentPane.add(btnNewVoiceOf);
		
		JLabel lblLoan = new JLabel("Loan");
		lblLoan.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLoan.setBounds(275, 249, 82, 24);
		contentPane.add(lblLoan);
		
		loantxt = new JTextField();
		loantxt.setColumns(10);
		loantxt.setBounds(362, 249, 135, 24);
		contentPane.add(loantxt);
		
		JLabel lblIsFinished = new JLabel("Is Finished");
		lblIsFinished.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIsFinished.setBounds(518, 348, 82, 24);
		contentPane.add(lblIsFinished);
		
		JLabel lblIdNewProject = new JLabel("Id new project");
		lblIdNewProject.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIdNewProject.setBounds(24, 21, 137, 24);
		contentPane.add(lblIdNewProject);
		
		idnewprojecttxt = new JTextField();
		idnewprojecttxt.setColumns(10);
		idnewprojecttxt.setBounds(157, 23, 135, 24);
		contentPane.add(idnewprojecttxt);
		
		//final 
		empcombo = new JComboBox();
		empcombo.setModel(new DefaultComboBoxModel(new String[] {"Select..."}));
		empcombo.removeAllItems();
		empcombo.addItem("Select...");
		

		empcombo.setBounds(132, 424, 135, 24);
		contentPane.add(empcombo);
		
		idnewphasetxt = new JTextField();
		idnewphasetxt.setColumns(10);
		idnewphasetxt.setBounds(132, 327, 135, 24);
		contentPane.add(idnewphasetxt);
		
		JLabel lblIdNewPhase = new JLabel("Id new phase");
		lblIdNewPhase.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIdNewPhase.setBounds(24, 325, 137, 24);
		contentPane.add(lblIdNewPhase);
		
		//final
		fasefinishedcombo = new JComboBox();
		fasefinishedcombo.setModel(new DefaultComboBoxModel(new String[] {"Select fase..."}));
		fasefinishedcombo.removeAllItems();
		fasefinishedcombo.addItem("Select...");
		fasefinishedcombo.setModel(new DefaultComboBoxModel(new String[] {"false", "true"}));
		fasefinishedcombo.setBounds(610, 376, 135, 24);
		contentPane.add(fasefinishedcombo);
		
		//final 
		final JButton btnSaveProject = new JButton("Save Project");

		btnSaveProject.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnSaveProject.setBounds(24, 687, 145, 24);
		contentPane.add(btnSaveProject);
		
		//final 
		final JButton btnSavePhase = new JButton("Save Phase");
		
		btnSavePhase.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnSavePhase.setBounds(202, 687, 145, 24);
		contentPane.add(btnSavePhase);
		
		JButton btnExitWithoutSave = new JButton("Exit without save");
		
		btnExitWithoutSave.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnExitWithoutSave.setBounds(561, 687, 184, 24);
		contentPane.add(btnExitWithoutSave);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setBounds(24, 663, 727, 2);
		contentPane.add(separator_4);
		
		JButton btnShowData = new JButton("Show Data");

		btnShowData.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnShowData.setBounds(518, 82, 123, 24);
		contentPane.add(btnShowData);
		


		
		
		
		//final 
		final JComboBox typeCombo = new JComboBox();
		typeCombo.setModel(new DefaultComboBoxModel(new String[] {"on line", "stand", "cartelloni", "evento", "stampa"}));
		
		typeCombo.setToolTipText("");
		typeCombo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		typeCombo.setBounds(362, 222, 135, 24);
		contentPane.add(typeCombo);
		
		//final 
		final JComboBox statusCombo = new JComboBox();
		statusCombo.setModel(new DefaultComboBoxModel(new String[] {"open", "closed", "finished"}));

		statusCombo.setToolTipText("");
		statusCombo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		statusCombo.setBounds(128, 222, 135, 24);
		contentPane.add(statusCombo);
		
		//final 
		customerCombo = new JComboBox();
	customerCombo.setModel(new DefaultComboBoxModel(new String[] {"Select..."}));
		customerCombo.removeAllItems();
		customerCombo.addItem("Select...");
		customerCombo.setToolTipText("");
		customerCombo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		customerCombo.setBounds(128, 173, 135, 24);
		contentPane.add(customerCombo);
		updateCustomerComboBox();
		
		fasefinishedtxt = new JTextField();
		fasefinishedtxt.setEditable(false);
		fasefinishedtxt.setColumns(10);
		fasefinishedtxt.setBounds(610, 350, 135, 24);
		contentPane.add(fasefinishedtxt);
		
		emptxt = new JTextField();
		emptxt.setColumns(10);
		emptxt.setBounds(132, 397, 135, 24);
		contentPane.add(emptxt);
		
		JLabel t45t645 = new JLabel("Project name");
		t45t645.setFont(new Font("Tahoma", Font.PLAIN, 16));
		t45t645.setBounds(24, 117, 98, 24);
		contentPane.add(t45t645);
		
		nameProjecttxt = new JTextField();
		nameProjecttxt.setColumns(10);
		nameProjecttxt.setBounds(128, 119, 135, 24);
		contentPane.add(nameProjecttxt);
		
		creationdatetxt = new JTextField();
		creationdatetxt.setColumns(10);
		creationdatetxt.setBounds(383, 119, 114, 24);
		contentPane.add(creationdatetxt);
		
		JLabel lblCreationDate = new JLabel("Creation date");
		lblCreationDate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCreationDate.setBounds(275, 117, 98, 24);
		contentPane.add(lblCreationDate);
		
		JLabel lblStandName = new JLabel("Stand name");
		lblStandName.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblStandName.setBounds(24, 275, 98, 24);
		contentPane.add(lblStandName);
		
		standnametxt = new JTextField();
		standnametxt.setColumns(10);
		standnametxt.setBounds(128, 277, 233, 24);
		contentPane.add(standnametxt);
		
		standlocationtxt = new JTextField();
		standlocationtxt.setColumns(10);
		standlocationtxt.setBounds(512, 277, 233, 24);
		contentPane.add(standlocationtxt);
		
		JLabel lblStandLocation = new JLabel("Stand location");
		lblStandLocation.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblStandLocation.setBounds(372, 277, 125, 24);
		contentPane.add(lblStandLocation);
		
		 showbutton = new JButton("Show Data");
		showbutton.setEnabled(false);

		showbutton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		showbutton.setBounds(621, 312, 123, 24);
		contentPane.add(showbutton);
		
		btnRefresh = new JButton("Refresh");
		btnRefresh.setEnabled(false);
	
		btnRefresh.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnRefresh.setBounds(495, 312, 123, 24);
		contentPane.add(btnRefresh);
		
		JButton button = new JButton("Show Data");
	
		button.setFont(new Font("Tahoma", Font.PLAIN, 16));
		button.setBounds(622, 494, 123, 24);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Refresh");
	
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		button_1.setBounds(495, 494, 123, 24);
		contentPane.add(button_1);
		
		JLabel lblDesc = new JLabel("Desc");
		lblDesc.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDesc.setBounds(446, 607, 82, 24);
		contentPane.add(lblDesc);
		
		desctxt = new JTextField();
		desctxt.setColumns(10);
		desctxt.setBounds(495, 609, 250, 24);
		contentPane.add(desctxt);
		
		//final 
		final JComboBox typecostocomboBox = new JComboBox();
		typecostocomboBox.setModel(new DefaultComboBoxModel(new String[] {"employee", "other"}));
		typecostocomboBox.setBounds(138, 631, 123, 24);
		contentPane.add(typecostocomboBox);
		
		List list = new List();
		list.setBounds(523, 21, 195, 24);
		contentPane.add(list);
		
		managerCombo.setEditable(true);
		
		JButton btnNewButton = new JButton("Refresh");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				updateProjects();
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnNewButton.setBounds(651, 82, 89, 24);
		contentPane.add(btnNewButton);
       
		if (_accessType.contentEquals("m")||_accessType.contentEquals("s")){
			btn1.setEnabled(true);
			btn2.setEnabled(true);
			btn3.setEnabled(true);
			btn4.setEnabled(true);
			btn5.setEnabled(true);
			btn6.setEnabled(true);
			
		}
		
		
		btn3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//cancellazione progetto
				
				if (!(_accessType.contentEquals("m")||_accessType.contentEquals("s"))){
				
					return;
				}
				
				ProjectInterface e = null;
				
					try {
						e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
					} catch (MalformedURLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (NotBoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				int idProject= Integer.valueOf(projectcombo.getSelectedItem().toString())  ;
				try {
					e.deleteProject(_connectionString, _dataUser, _dataPwd, idProject);
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		
		
	/*	projectcombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//visualizzo i dati del progetto selezionato
				ProjectInterface e = null;
				
				try {
					e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Project project=e.getProject(_connectionString, _dataUser, _dataPwd, Integer.valueOf(projectcombo.getSelectedItem().toString()));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				
			}
		});
	*/	
		
		projectcombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				//progetto selezionato
				
				btnRefresh.setEnabled(false);
				showbutton.setEnabled(false);
			}
		});
		
		btnExitWithoutSave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//exit without save
				
				exit();
				
			}
		});
		
		btnSavePhase.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// salvo dati fase
				
				btnSavePhase.setEnabled(false);
				PhaseInterface e = null;
				
				
				try {
					e = (PhaseInterface)Naming.lookup(_address+"/PhaseInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Phase phase=null;
				phase.setDeadline(deadlinetxt.getText());
				phase.setFinished(Boolean.valueOf(fasefinishedtxt.getText()) );
				phase.setDipendente(emptxt.getText());
				
				try {
					e.updatePhase(_connectionString, _dataUser, _dataPwd,  Integer.valueOf((String)projectcombo.getSelectedItem()),phase );
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					e.updatePhase(_connectionString, _dataUser, _dataPwd, Integer.valueOf((String)projectcombo.getSelectedItem()), phase);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatephaselist();
				btnSavePhase.setEnabled(true);
			}
		});
		
		
		btnSaveProject.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//salvo dati progetto
				btnSaveProject.setEnabled(false);
				ProjectInterface e = null;
				
			
					try {
						e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
					} catch (MalformedURLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (NotBoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					Project project=new Project((Integer) projectcombo.getSelectedItem(),nameProjecttxt.getText(), creationdatetxt.getText(), (String)customerCombo.getSelectedItem(), deadlinetxt.getText(), (String)managerCombo.getSelectedItem(), (String)typeCombo.getSelectedItem());
					
					
					try {
						if (statustxt.getText().contentEquals("closed")||e.isFinishedCalculate(_connectionString, _dataUser, _dataPwd, (Integer)projectcombo.getSelectedItem())){
							project.setClosed(true);
							project.setFinished(true);
						}
					} catch (NumberFormatException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					} catch (RemoteException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					if (statustxt.getText().contentEquals("finished")){
						
						project.setFinished(true);
					}
					
			

					double ricavo=0;
					ProjectInterface eee = null;
					
					
					try {
						eee = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
					} catch (MalformedURLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (NotBoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						ricavo= eee.getSellPrice(_connectionString, _dataUser, _dataPwd,Integer.valueOf( (Integer) projectcombo.getSelectedItem()),customertxt.getText() );
					} catch (NumberFormatException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					
					project.setRicavo(ricavo);
					
					
					try {
						e.updateProject(_connectionString, _dataUser, _dataPwd, project);
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					/*
					try {
						e.updateProject(_connectionString, _dataUser, _dataPwd, project);
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}*/
					updateProjects();
					btnSaveProject.setEnabled(true);
			}
		});

		typecostocomboBox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				// aggiorno txt tipo di costo
				typecosttxt.setText((String)typecostocomboBox.getSelectedItem());
				
			}
		});
		
		btn6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//delete voice of cost
			int id=	(Integer) costcombo.getSelectedItem();
			int idPhase=(Integer)phasecombo.getSelectedItem();
			int idProject=(Integer)projectcombo.getSelectedItem();
			CostInterface e = null;
			
		
				try {
					e = (CostInterface)Naming.lookup(_address+"/CostInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				
				try {
					e.deleteVoiceOfCost(_connectionString, _dataUser, _dataPwd, idProject, idPhase, id);
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//mostra elenco costi
				CostInterface e = null;
				
				try {
					e = (CostInterface)Naming.lookup(_address+"/CostInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				
				ArrayList<Integer> costList=new ArrayList<Integer>();
				for (int i=0; i<costList.size(); i=i+1){
				costList.remove(i);
				}
				try {
					costList=e.getIdCost(_connectionString, _dataUser, _dataPwd, (Integer)projectcombo.getSelectedItem(),(Integer)phasecombo.getSelectedItem());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				costcombo.removeAllItems();
				
				for (int i=0; i<costList.size(); i=i+1){
					costcombo.addItem(costList.get(i));
				}
				
			}
		});
		
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				//show voice of cost
				VoiceOfcost voc=null;
				
				CostInterface k = null;
				try {
					k = (CostInterface)Naming.lookup(_address+"/CostInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				;		
				try {
					 voc= k.getCostById(_connectionString, _dataUser, _dataPwd,(Integer)costcombo.getSelectedItem() , (Integer)phasecombo.getSelectedItem(),(Integer)projectcombo.getSelectedItem());
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (voc!=null){
				typecosttxt.setText(voc.get_type());
				amounttxt.setText(String.valueOf(voc.get_amount()) );
				desctxt.setText(voc.get_description());
				}
				
				
			}
		});
		
		btnNewVoiceOf.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				// nuova voce di costo int idProject, int idPhase, String address, String connectionString, String dataUser, String dataPwd)
				CreateCost createcost= new CreateCost((Integer)projectcombo.getSelectedItem(),(Integer)phasecombo.getSelectedItem(),_address, _connectionString, _dataUser, _dataPwd);
				createcost.setVisible(true);
				
			}
		});
		
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			//delete phase
				if (!(_accessType.contentEquals("m")||_accessType.contentEquals("s"))){
					return;
				}
				
				PhaseInterface k = null;
				try {
					k = (PhaseInterface)Naming.lookup(_address+"/PhaseInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				;
				
				try {
					k.deletePhase(_connectionString, _dataUser, _dataPwd, (Integer)phasecombo.getSelectedItem()	, (Integer)projectcombo.getSelectedItem());
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
		
		btn4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//CREA LA FASE
				PhaseInterface k = null;
				try {
					k = (PhaseInterface)Naming.lookup(_address+"/PhaseInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				;
				try {
					k.addPhase(_connectionString, _dataUser, _dataPwd, 
						Integer.valueOf(idnewphasetxt.getText()), (Integer)projectcombo.getSelectedItem(), emptxt.getText(), deadfasetxt.getText(), false);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
		
		
		fasefinishedcombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
	//AGGIORNO txt 
				
				fasefinishedtxt.setText((String)fasefinishedcombo.getSelectedItem());
			}
		});
		
		empcombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				//AGGIORNO txt
				
				emptxt.setText((String)empcombo.getSelectedItem());
			}
		});
		
		btnShowDeatils.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				// mostra i dettagli del progetto
				//Integer idproject, String type, String connectionString, String dataUser, String dataPwd, String address
				
				try {
					DetailsWindow details = new DetailsWindow((Integer)projectcombo.getSelectedItem(),typetxt.getText(),_connectionString, _dataUser, _dataPwd, _address);
					details.setVisible(true);
					details.setExtendedState(JFrame.MAXIMIZED_BOTH);
					
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
					
				
				
			}
		});
		
		
		showbutton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				if (!showbutton.isEnabled()){
					return;
				}
				//visualizzo dati fase
				PhaseInterface k = null;
				try {
					k = (PhaseInterface)Naming.lookup(_address+"/PhaseInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				Phase phase = null;
				
				try {
					phase=k.getPhase(_connectionString, _dataUser, _dataPwd, (Integer)projectcombo.getSelectedItem() , (Integer)phasecombo.getSelectedItem());   //getPhaseList(_connectionString, _dataUser, _dataPwd, Integer.valueOf((String)projectcombo.getSelectedItem()));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		if (phase!=null){
				System.out.println("get dipendente fase..."+phase.getDipendente());
		emptxt.setText(phase.getDipendente());
		deadfasetxt.setText(phase.getDeadline());
		fasefinishedtxt.setText(String.valueOf(phase.isFinished())  );
		CostInterface n = null;
		
		try {
			n = (CostInterface)Naming.lookup(_address+"/CostInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		double employeeCost = 0;
		try {
			employeeCost = n.getEmployeeCostOfPhase(_connectionString, _dataUser, _dataPwd, (Integer)projectcombo.getSelectedItem(), (Integer)phasecombo.getSelectedItem());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		employeecosttxt.setText(Double.toString(employeeCost));
		
		double otherCost = 0;
		try {
			employeeCost = n.getOtherCostOfPhase(_connectionString, _dataUser, _dataPwd, (Integer)projectcombo.getSelectedItem(), (Integer)phasecombo.getSelectedItem());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		othercosttxt.setText(Double.toString(otherCost));
		totcosttxt.setText(Double.toString(otherCost+employeeCost));
			}
		
			}
		});
		
		
		
		
		btnRefresh.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// refresh

			updatephaselist();
				
			}


		});
		
		btn2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			//close project	
				ProjectInterface e = null;
				
				try {
					e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
				String idproject=(String)projectcombo.getSelectedItem();
				try {
					boolean b=e.setClosed(_connectionString, _dataUser, _dataPwd,Integer.valueOf(idproject) , true);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		
		
		btn1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				//nuovo progetto
			ProjectInterface e=null;
			try {
				e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NotBoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String type=(String)typeCombo.getSelectedItem();
			Date data=new Date();
			String creationDate=data.toString();
			Project project;
			
			if (type.contentEquals("stand") ){
				
				
				 project=new StandProject(Integer.valueOf(idnewprojecttxt.getText()), nameProjecttxt.getText(),creationDate ,customertxt.getText(), deadlinetxt.getText(), managertxt.getText(), (String)typeCombo.getSelectedItem(),standnametxt.getText(),standlocationtxt.getText());
				
			}
			else {
				
				 project=new Project(Integer.valueOf(idnewprojecttxt.getText()), nameProjecttxt.getText(),creationDate ,customertxt.getText(), deadlinetxt.getText(), managertxt.getText(), (String)typeCombo.getSelectedItem());
						
			}
			
			try {
				e.addProject(_connectionString, _dataUser, _dataPwd, project);
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				
			}
		});
		
		
		customerCombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				//AGGIORNO txt customer quando clicco su combobox
				
				customertxt.setText((String)customerCombo.getSelectedItem());
				
			}
		});
		
		statusCombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			//AGGIORNO txt status quando clicco su combobox
				
				statustxt.setText((String)statusCombo.getSelectedItem());
			}
		});
		
		typeCombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			//AGGIORNO txt type quando clicco su combobox
				
				typetxt.setText((String)typeCombo.getSelectedItem());	
				
			}
		});
		
		managerCombo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			//AGGIORNO txt manager quando clicco su combobox
				
				managertxt.setText((String)managerCombo.getSelectedItem());	
			}
		});
		
		btnShowData.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.out.println("entra in show data ");
				//mostro dati progetto selezionato
				btnRefresh.setEnabled(true);
				
				ProjectInterface e = null;
				//OrderInterface_ f=null;
				//Project project=new Project( (Integer)projectcombo.getSelectedItem(), "name", "creation", "costumer", "deadline", "manager", "tipo");
				try {
					e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				

					
					//Project project;
					try {
					Project	project = e.getProject(_connectionString, _dataUser, _dataPwd, (Integer)projectcombo.getSelectedItem());

					
	
				// aggiorno caselle di testo
				
				customertxt.setText(project.getCostumer());
				deadlinetxt.setText(project.getDeadline());
				managertxt.setText(project.getManager());
				nameProjecttxt.setText(project.getNameProject());
				creationdatetxt.setText(project.getCreationDate());
				
				typetxt.setText(project.getTipo());
				if (project.getTipo()!=null){
				System.out.println("project get tipo "+project.getTipo());
				if(project.getTipo().contentEquals("stand")){
					standlocationtxt.setText(project.get_location());
					standnametxt.setText(project.get_nome());
				}
				else{
					standlocationtxt.setText("");
					standnametxt.setText("");
				}
				}
				
				String status=null;
				if (project.isClosed()){
					status="closed";
				}
					else{
						try {
							if ( e.isFinishedCalculate(_connectionString, _dataUser, _dataPwd, Integer.valueOf(projectcombo.getSelectedItem().toString()))) {
							status="finished";
}
							else{
								status="open";
							
}
						} catch (NumberFormatException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (RemoteException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				}
				
				statustxt.setText(status);
				
				double totCost = 0;
				try {
					totCost = e.getTotCosts(_connectionString, _dataUser, _dataPwd, Integer.valueOf(projectcombo.getSelectedItem().toString()));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				coststxt.setText(Double.toString(totCost));
				try {
					e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				double loan = 0;
				try {
					loan = e.getSellPrice(_connectionString, _dataUser, _dataPwd, Integer.valueOf(projectcombo.getSelectedItem().toString()), project.getCostumer());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				loantxt.setText(Double.toString(loan));
				
				CostInterface o = null;
				double materialCost=0;
				
				try {
					o = (CostInterface)Naming.lookup(_address+"/CostInt");
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					materialCost=o.getMaterialCost(_connectionString, _dataUser, _dataPwd, Integer.valueOf(projectcombo.getSelectedItem().toString()));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				materialcosttxt.setText(Double.toString(materialCost));
				
					} catch (RemoteException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					} catch (SQLException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				updateDipendentiComboBox();
				
		
			}
		});
	
	
	
	}
	
	protected void exit() {
		// TODO Auto-generated method stub
		this.setVisible(false);
	}

	private void updateCustomerComboBox(){
		
		String[] customers=null;
		CustomerInterface g = null;
		try {
			g = (CustomerInterface)Naming.lookup(_address+"/CustomerInt");
			
		//-------------------------------------------------------
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ArrayList<String> cust = new ArrayList<String>();
		
		try {
			cust= g.getCustomersStringList(_connectionString, _dataUser, _dataPwd);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		if(customerCombo.getItemCount()!=0){
			customerCombo.removeAllItems();
		}
		
		for (int i=0; i<cust.size(); i=i+1){
			
			customerCombo.addItem(cust.get(i));
			
		}	
		
		
		
		
	}
	
	

	private void updateManagerComboBox() {
		// TODO Auto-generated method stub
		ArrayList<String> managers=new ArrayList<String>();
		AdminInterface f = null;
		try {
			f = (AdminInterface)Naming.lookup(_address+"/AdminInt");
			
		//-------------------------------------------------------
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			managers=f.getDipendentiManager(_connectionString, _dataUser, _dataPwd, 'm');
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		if(managerCombo!=null){
		if(( managerCombo).getItemCount()!=0){
			( managerCombo).removeAllItems();
		}
		}
		/*if (managerString!=null){
		for (int i=0; i<managerString.size(); i=i+1){
		managerString.remove(i);
		}
		}*/
		if (managers!=null){
			if(managers.size()!=0){
		for (int i=0; i<managers.size(); i=i+1){
			System.out.println("manager i "+managers.get(i));
			/*managerString.add(managers.get(i));
			list.add(managers.get(i));*/
			managerCombo.addItem(managers.get(i));
							
		}
		}
		}
		/*
		managerCombo = new JComboBox();// managers.get(i));	
		managerCombo.addItem(managerString);*/
	}
	
	
	


	@SuppressWarnings("unchecked")
	void updateProjects(){
		
		ProjectInterface e = null;
		try {
			e = (ProjectInterface)Naming.lookup(_address+"/ProjectInt");
			
		//-------------------------------------------------------
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ArrayList<Integer> _listIdProjects=new ArrayList<Integer>();
		try {
			_listIdProjects= e.getIdProjectsList(_connectionString, _dataUser, _dataPwd);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("_listIdProjects " +_listIdProjects);
		if (projectcombo.getComponentCount()!=0){
			projectcombo.removeAllItems();
		}
		if (_listIdProjects!=null){
		for (int i=0; i<_listIdProjects.size(); i=i+1){
			System.out.println(_listIdProjects.get(i));
			projectcombo.addItem(_listIdProjects.get(i));
		}
		}
	}
	
	private void updatephaselist() {
		// TODO Auto-generated method stub
		if (!btnRefresh.isEnabled() ){
			return;
		}
		phasecombo.removeAll();	
		PhaseInterface k = null;
		try {
			k = (PhaseInterface)Naming.lookup(_address+"/PhaseInt");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		ArrayList<Integer> phases = new ArrayList<Integer>();
		
		try {
			phases=k.getPhaseList(_connectionString, _dataUser, _dataPwd, (Integer)projectcombo.getSelectedItem());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		phasecombo.removeAllItems();
		for (int i=0; i<phases.size(); i=i+1){
			phasecombo.addItem(phases.get(i));
			
		}
	}
	
	private void updateDipendentiComboBox() {
		// TODO Auto-generated method stub
		ArrayList<String> dipendenti= new ArrayList<String>();
		AdminInterface f = null;
		try {
			f = (AdminInterface)Naming.lookup(_address+"/AdminInt");
			
		//-------------------------------------------------------
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NotBoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			dipendenti=f.getDipendentiManager(_connectionString, _dataUser, _dataPwd, 'd');
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		if(empcombo!=null){
		if(( empcombo).getItemCount()!=0){
			( empcombo).removeAllItems();
		}
		}
	
		
		if (dipendenti!=null){
			if(dipendenti.size()!=0){
		for (int i=0; i<dipendenti.size(); i=i+1){
			//System.out.println("manager i "+managers.get(i));
			/*managerString.add(managers.get(i));
			list.add(managers.get(i));*/
			empcombo.addItem(dipendenti.get(i));
							
		}
		}
		}
		/*
		managerCombo = new JComboBox();// managers.get(i));	
		managerCombo.addItem(managerString);*/
	
	}
}


