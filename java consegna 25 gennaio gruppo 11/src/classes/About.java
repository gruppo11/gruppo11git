package classes;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class About extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					About frame = new About();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public About() {
		setTitle("Gruppo 11 Sweng");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 224);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ingegneria del Software, progetto d'anno - Gennaio 2017 - GRUPPO 11");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(30, 24, 557, 24);
		contentPane.add(lblNewLabel);
		
		JLabel lblMassimoBorroni = new JLabel("Massimo Borroni");
		lblMassimoBorroni.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMassimoBorroni.setBounds(30, 59, 498, 24);
		contentPane.add(lblMassimoBorroni);
		
		JLabel lblMatteoPiccinno = new JLabel("Matteo Piccinno");
		lblMatteoPiccinno.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMatteoPiccinno.setBounds(30, 94, 498, 24);
		contentPane.add(lblMatteoPiccinno);
		
		JLabel lblLucaDeiRossi = new JLabel("Luca Dei Rossi");
		lblLucaDeiRossi.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLucaDeiRossi.setBounds(30, 129, 498, 24);
		contentPane.add(lblLucaDeiRossi);
		
		JButton btnNewButton = new JButton("Ok");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				closewin();
			}
		});
		btnNewButton.setBounds(498, 132, 89, 23);
		contentPane.add(btnNewButton);
	}

	protected void closewin() {
		// TODO Auto-generated method stub
		this.setVisible(false);
	}
}
