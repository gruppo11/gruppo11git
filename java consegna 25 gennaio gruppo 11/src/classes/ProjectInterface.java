package classes;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface ProjectInterface extends Remote {

	public boolean addProject(String connectionString, String dataUser, String dataPassword, Project project) throws RemoteException;
	public  ArrayList<Integer> getIdProjectsList(String connectionString, String dataUser,String dataPwd)throws RemoteException;
	public boolean deleteProject(String connectionString, String dataUser,String dataPwd, int idProject)throws RemoteException;
	public Project getProject (String connectionString, String dataUser,String dataPwd, int idProject)throws RemoteException, SQLException;
	public double getTotCosts (String connectionString, String dataUser,String dataPwd, int idProject)throws RemoteException;
	public boolean setFinished (String connectionString, String dataUser,String dataPwd, int idProject, boolean isFinished)throws RemoteException;
	public boolean setClosed (String connectionString, String dataUser,String dataPwd, int idProject, boolean isclosed)throws RemoteException;
	public boolean updateProject(String connectionString, String dataUser,String dataPwd, Project project)throws RemoteException;
	public boolean isFinishedCalculate (String connectionString, String dataUser,String dataPwd, int idProject)throws RemoteException;
	public boolean setMaterialeProgetto(String connectionString, String dataUser,String dataPwd, int idProject, int idMateriale, double qta)throws RemoteException;
	
	
	public boolean addOrder (String connectionString, String dataUser, String dataPassword, int idProject, String idCustomer, double sellPrice, String status) throws RemoteException;
	public boolean modifyOrder (String connectionString, String dataUser, String dataPassword, int idProject,String idCustomer, double sellPrice, String status) throws RemoteException;
	public double  getSellPrice(String connectionString, String dataUser, String dataPassword, int idProject,String idCustomer)throws RemoteException;
	public Order getOrder (String connectionString, String dataUser, String dataPassword, int idProject,String idCustomer)throws RemoteException;
}


