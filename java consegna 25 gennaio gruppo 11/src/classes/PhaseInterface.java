package classes;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface PhaseInterface extends Remote{
	
	public boolean addPhase (String connectionString, String dataUser, String dataPassword, int phaseId, int projectId, String dipendente, String deadline, boolean isFinished) throws RemoteException;
	public boolean deletePhase (String connectionString, String dataUser, String dataPassword, int phaseId, int projectId) throws RemoteException;
	public ArrayList<Integer> getPhaseList (String connectionString, String dataUser, String dataPassword, int projectId) throws RemoteException;
	public Phase getPhase(String connectionString, String dataUser, String dataPassword, int projectId, int phaseId) throws RemoteException;
	public boolean updatePhase (String connectionString, String dataUser, String dataPassword, int projectId, Phase phase) throws RemoteException;
	
}


