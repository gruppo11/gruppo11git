package classes;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.sql.*;


public interface LoginInterface extends Remote {

    
    public String setLoginFromData(String connectionString, String dataUser, String dataPwd, String user, char[] pwd, int pwdAcc) throws RemoteException;
    public String getConnectString (String host, String dataName)throws RemoteException;
   
}
