package classes;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;



public interface CostInterface extends Remote {

	public boolean addVoiceOfCost(String connectionString, String dataUser, String dataPassword, int idProject, int idPhase, int idVoiceOfCost, double amount, String type, String description) throws RemoteException;
	public boolean deleteVoiceOfCost (String connectionString, String dataUser, String dataPassword, int idProject, int idPhase, int idVoiceOfCost)throws RemoteException;
	public double getTotalCost(String connectionString, String dataUser, String dataPassword, int idProject)throws RemoteException;
	public double getMaterialCost (String connectionString, String dataUser, String dataPassword, int idProject)throws RemoteException;
	public double getEmployeeCostOfPhase (String connectionString, String dataUser, String dataPassword, int idProject, int idPhase)throws RemoteException;
	public double getOtherCostOfPhase (String connectionString, String dataUser, String dataPassword, int idProject, int idPhase)throws RemoteException;
	public ArrayList<Integer> getIdCost(String connectionString, String dataUser, String dataPassword, int idProject, int idPhase)throws RemoteException;
	public VoiceOfcost getCostById (String connectionString, String dataUser, String dataPassword, int idVoiceOfCost, int idPhase, int idProject)throws RemoteException;
}











