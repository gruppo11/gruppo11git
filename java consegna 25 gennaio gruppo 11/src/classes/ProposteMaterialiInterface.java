package classes;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface ProposteMaterialiInterface extends Remote {

	public boolean addMaterialeProgetto(String connectionString, String dataUser, String dataPassword, int idProject, int qta, int idMateriale) throws RemoteException;
	public boolean addFormatoProposta(String connectionString, String dataUser, String dataPassword, int idProject, int idProposta, String proposta, String formato) throws RemoteException;
	public boolean addPropostaStand(String connectionString, String dataUser, String dataPassword, int idProject, int idProposta, int capienza, int metratura, String link, String luogo) throws RemoteException, SQLException;
	public boolean addServizi(String connectionString, String dataUser, String dataPassword, int idProject, int idServizio,  String servizio) throws RemoteException;
	public boolean addPersonale(String connectionString, String dataUser, String dataPassword, int idProject, int idPersonale,  String ruolo) throws RemoteException;
	public boolean addProgramma(String connectionString, String dataUser, String dataPassword, int idProject, int idProgramma, String prog) throws RemoteException;
	public boolean addMateriale(String connectionString, String dataUser, String dataPassword, int idMateriale, double costo, String nome) throws RemoteException;
	public ArrayList<Integer> getListaMatprogetto (String connectionString, String dataUser, String dataPassword, int idProject) throws RemoteException;
	public ArrayList<Integer> getListaProposte(String connectionString, String dataUser, String dataPassword, int idProject) throws RemoteException;
	public ArrayList<Integer> getListaProposteStand (String connectionString, String dataUser, String dataPassword, int idProject) throws RemoteException;
	public ArrayList<Integer> getMateriali (String connectionString, String dataUser, String dataPassword) throws RemoteException;
	public ArrayList<String> getProgramma(String connectionString, String dataUser, String dataPassword, int idProject) throws RemoteException;
	public ArrayList<String> getServizi(String connectionString, String dataUser, String dataPassword, int idProject) throws RemoteException;
	public ArrayList<String> getPersonale(String connectionString, String dataUser, String dataPassword, int idProject) throws RemoteException;
	public boolean deleteMaterialeProgetto(String connectionString, String dataUser, String dataPassword, int idProject, int idMateriale) throws RemoteException;
	public boolean deleteMateriale(String connectionString, String dataUser, String dataPassword, int idMateriale) throws RemoteException;
	public boolean deleteProposta(String connectionString, String dataUser, String dataPassword, int idProject, int idProposta) throws RemoteException;
	public boolean deletePropostaStand(String connectionString, String dataUser, String dataPassword, int idProject, int idProposta) throws RemoteException;
	public boolean deleteProgramma(String connectionString, String dataUser, String dataPassword, int idProject, int idProgramma) throws RemoteException;
	public boolean deleteServizio(String connectionString, String dataUser, String dataPassword, int idProject, int idServizio) throws RemoteException;
	public boolean deletePersonale(String connectionString, String dataUser, String dataPassword, int idProject, int idPersonale) throws RemoteException;
	public int getQtaMatProgetto(String connectionString, String dataUser, String dataPassword, int idProject, int idMateriale) throws RemoteException;
	
	public Proposta getproposta(String connectionString, String dataUser, String dataPassword, int idProject, int idProposta) throws RemoteException;
	public PropostaStand getpropostaStand(String connectionString, String dataUser, String dataPassword, int idProject, int idProposta) throws RemoteException;
}
