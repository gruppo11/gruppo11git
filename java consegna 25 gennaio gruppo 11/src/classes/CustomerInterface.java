package classes;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface CustomerInterface extends Remote, Serializable{
	
	 public boolean addCustomer(String connectionString, String dataUser, String dataPwd, Customer customer) throws RemoteException;
	 public boolean deleteCustomer(String connectionString, String dataUser, String dataPwd, String idcustomer) throws RemoteException;
	 public String getCustomers(String connectionString, String dataUser, String dataPwd)throws RemoteException;
	 public ArrayList<String> getCustomersStringList(String connectionString, String dataUser, String dataPwd)throws RemoteException;
	 public String prova(String connectionString, String dataUser, String dataPwd)throws RemoteException;
}

