package classes;

import java.io.Serializable;
import java.util.ArrayList;


public class PropostaStand implements Serializable{
	
	private int idPropostaStand;
	private int idProject;
	private String link;
	private String metratura;
	private String capienza;
	private String luogo;

	
	



	public int getIdPropostaStand() {
		return idPropostaStand;
	}
	public void setIdPropostaStand(int idPropostaStand) {
		this.idPropostaStand = idPropostaStand;
	}
	public int getIdProject() {
		return idProject;
	}
	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getMetratura() {
		return metratura;
	}
	public void setMetratura(String metratura) {
		this.metratura = metratura;
	}
	public String getCapienza() {
		return capienza;
	}
	public void setCapienza(String capienza) {
		this.capienza = capienza;
	}
	public String getLuogo() {
		return luogo;
	}
	public void setLuogo(String luogo) {
		this.luogo = luogo;
	}
	

	
	public PropostaStand(int idPropostaStand_, int idProject_, String link_, String metratura_, String capienza_){
		
		idProject=idProject_;
		idPropostaStand=idPropostaStand_;
		link=link_;
		metratura=metratura_;
		capienza=capienza_;

	}
	


}
