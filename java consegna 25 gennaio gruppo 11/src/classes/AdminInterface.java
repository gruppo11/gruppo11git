package classes;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.*;
import java.util.ArrayList;



public interface AdminInterface extends Remote {


    public boolean writeOnUsersSql (String connectionString, String dataUser, String dataPassword, String user, String pwd, String access)throws RemoteException;
    public boolean deleteOnUserSql (String connectionString, String dataUser, String dataPassword, String user)throws RemoteException;
    public ArrayList<String> getDipendentiManager(String connectionString, String dataUser, String dataPassword, char dipMan)throws RemoteException;    
    public boolean setExtPrivileges(String connectionString, String dataUser, String dataPassword,String iduser, int maxAcc) throws RemoteException;
    public boolean setExtAccess(String connectionString, String dataUser, String dataPassword,String iduser, int acc) throws RemoteException;
    public int getExtAccess(String connectionString, String dataUser, String dataPassword,String iduser) throws RemoteException;
    public int getMaxExtAccess(String connectionString, String dataUser, String dataPassword,String iduser) throws RemoteException;
    public ArrayList<String> getExtUser(String connectionString, String dataUser, String dataPassword)throws RemoteException;    
    public boolean deleteAllTablesInDatabase(String connectionString, String dataUser, String dataPassword)throws RemoteException;    

}


